/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package map;

import entity.Enemy;
import entity.PlayerData;
import halloween.game.jam.MainFrame;
import java.awt.Graphics;
import java.awt.Point;
import java.util.ArrayList;
import java.util.Iterator;
import libraries.Element;
import libraries.Rectangle;
import prop.Prop;

/**
 *
 * @author Nikhil
 */
public abstract class Map {
    
    // Global Variables
    public int gameWidth, gameHeight;
    public ArrayList<Rectangle> platforms = new ArrayList<>();
    public ArrayList<Prop> props = new ArrayList<>();
    public ArrayList<Element> overlay = new ArrayList<>();
    public ArrayList<Enemy> enemies = new ArrayList<>();
    public Point playerStartPos;
    
    public PlayerData playerData = new PlayerData();
    
    public boolean isLoaded = false;
    
    
    
    
    public Map(int newGameWidth, int newGameHeight) {
        initMap();
        
        gameWidth = newGameWidth;
        gameHeight = newGameHeight;
        
        playerStartPos = new Point(newGameWidth/2, newGameHeight/2);
    }
    public Map() {
        initMap();
        
        gameWidth = MainFrame.GAME_WIDTH;
        gameHeight = MainFrame.GAME_HEIGHT;
        
        playerStartPos = new Point(gameWidth/2, gameHeight/2);
    }
    
    
    
    
    
    public abstract void init();
    
    
    
    
    
    public void initMap() {
        platforms = new ArrayList<>();
        props = new ArrayList<>();
        overlay = new ArrayList<>();
        enemies = new ArrayList<>();
    }
    
    public void draw(Graphics g) {
        //if (isLoaded) {
            Iterator platformsIterator = platforms.iterator();
            Iterator propsIterator = props.iterator();
            Iterator overlayIterator = overlay.iterator();
            Iterator enemiesIterator = enemies.iterator();

            while (platformsIterator.hasNext()) {
                ((Rectangle) platformsIterator.next()).draw(g);
            }

            while (propsIterator.hasNext()) {
                ((Prop) propsIterator.next()).draw(g);
            }

            while (overlayIterator.hasNext()) {
                ((Element) overlayIterator.next()).draw(g);
            }

            while (enemiesIterator.hasNext()) {
                ((Enemy) enemiesIterator.next()).draw(g);
            }
        //}
    }
    
    public void update() {
        for (int i = 0; i < props.size(); i ++) {
            props.get(i).update();
        }
        
        for (int i = 0; i < enemies.size(); i ++) {
            enemies.get(i).update();
        }
    }
    
    public boolean isColliding(Element e) {
        int leftSide, rightSide, top, bottom;
        
        int eLeftSide = e.getX();
        int eRightSide = e.getX() + e.getWidth();
        int eTop = e.getY();
        int eBottom = e.getY() + e.getHeight();

        for (Rectangle platform : platforms) {
            leftSide = platform.getX();
            rightSide = platform.getX() + platform.getWidth();
            top = platform.getY();
            bottom = platform.getY() + platform.getHeight();
            
            if (eLeftSide < rightSide) {
                if (eRightSide > leftSide) {
                    if (eTop < bottom) {
                        if (eBottom > top) {
                            return true;
                        }
                    }
                }
            }
        }
        
        
        
        return false;
    }
    
    
    
    
    // Getters and Setters
    public ArrayList<Rectangle> getPlatforms() {
        return platforms;
    }
    
    /**
     * Should only be called after isColliding() returns true
     * @param e
     * @return 
     */
    public ArrayList<Rectangle> getColliding(Element e) {
        ArrayList<Rectangle> ret = new ArrayList<>();
        
        int leftSide, rightSide, top, bottom;
        
        int eLeftSide = e.getX();
        int eRightSide = e.getX() + e.getWidth();
        int eTop = e.getY();
        int eBottom = e.getY() + e.getHeight();

        for (Rectangle platform : platforms) {
            leftSide = platform.getX();
            rightSide = platform.getX() + platform.getWidth();
            top = platform.getY();
            bottom = platform.getY() + platform.getHeight();
            
            if (eLeftSide < rightSide) {
                if (eRightSide > leftSide) {
                    if (eTop < bottom) {
                        if (eBottom > top) {
                            ret.add(platform);
                        }
                    }
                }
            }
        }
        
        
        return ret;
    }
    
}
