/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package map;

import entity.Enemy;
import entity.PlayerData;
import halloween.game.jam.MainFrame;
import java.awt.Color;
import java.awt.Point;
import libraries.Rectangle;
import prop.Door;
import prop.Doors;
import prop.FrontDoor;
import prop.Lamp;
import prop.Plant;
import prop.Pot;
import prop.PottedPlant;
import prop.SmallDresser;
import prop.SmallTable;

/**
 *
 * @author Nikhil
 */
public class Map1 extends Map {
    
    public Map1() {
        super(MainFrame.GAME_WIDTH, MainFrame.GAME_HEIGHT);
        
        init();
        playerStartPos = new Point(120, gameHeight-(gameHeight/2)-PlayerData.HEIGHT);
    }
    
    
    

    @Override
    public void init() {
        platforms.add(new Rectangle(100, gameHeight-(gameHeight/2), gameWidth-200, 10));
        platforms.add(new Rectangle(100, gameHeight-(gameHeight/2)-210, gameWidth-200, 10));
        platforms.add(new Rectangle(100, gameHeight-(gameHeight/2)+210, gameWidth-200, 10));
        
        platforms.add(new Rectangle(90, gameHeight-(gameHeight/2)-210, 10, 430));
        platforms.add(new Rectangle(gameWidth-100, gameHeight-(gameHeight/2)-210, 10, 430));
        
        
        
        
        props.add(new FrontDoor(gameWidth-100-Door.WIDTH, gameHeight-(gameHeight/2)+10, false));
        props.add(new Doors(gameWidth-100-Door.WIDTH, gameHeight-(gameHeight/2)-Door.HEIGHT, false, 
                            100, gameHeight-(gameHeight/2)+10, false));
        props.add(new SmallDresser(gameWidth/2-(SmallDresser.WIDTH/2), gameHeight-(gameHeight/2)-SmallDresser.HEIGHT));
        props.add(new SmallTable(gameWidth/2+(gameWidth/4), gameHeight-(gameHeight/2)-SmallTable.HEIGHT));
        props.add(new Lamp(gameWidth/2+(gameWidth/4), gameHeight-(gameHeight/2)-SmallTable.HEIGHT-Lamp.HEIGHT));
        props.add(new SmallDresser(gameWidth/2-(gameWidth/4), gameHeight-(gameHeight/2)+210-SmallDresser.HEIGHT));
        props.add(new PottedPlant(gameWidth/2-(gameWidth/8), gameHeight-(gameHeight/2)+210-Pot.HEIGHT, 
                                  gameWidth/2-(gameWidth/8)-Plant.OFFSET, gameHeight-(gameHeight/2)+210-Pot.HEIGHT-Plant.HEIGHT));
        props.add(new PottedPlant(gameWidth/2+(gameWidth/8), gameHeight-(gameHeight/2)+210-Pot.HEIGHT, 
                                  gameWidth/2+(gameWidth/8)-Plant.OFFSET, gameHeight-(gameHeight/2)+210-Pot.HEIGHT-Plant.HEIGHT));
        
        
        
        
        int[] enemyMovement = {Enemy.MOVE_LEFT, gameWidth/2-(gameWidth/4)+93, Enemy.MOVE_RIGHT, gameWidth-250};
        enemies.add(new Enemy(gameWidth-250, gameHeight-(gameHeight/2)+210-140, 0, enemyMovement));
        
        
        
        
        for (int i = 0; i < platforms.size(); i ++) {
            platforms.get(i).setColor(Color.BLACK);
        }
        
        isLoaded = true;
    }
    
}
