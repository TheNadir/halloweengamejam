/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package map;

import entity.Enemy;
import entity.PlayerData;
import halloween.game.jam.MainFrame;
import java.awt.Color;
import java.awt.Point;
import libraries.Rectangle;
import prop.Bed;
import prop.Bookshelf;
import prop.Chair;
import prop.Computer;
import prop.Door;
import prop.Doors;
import prop.FrontDoor;
import prop.Key;
import prop.Lamp;
import prop.LargeDresser;
import prop.LargeTable;
import prop.Plant;
import prop.Pot;
import prop.PottedPlant;
import prop.SmallTable;

/**
 *
 * @author Nikhil
 */
public class Map2 extends Map {
    
    public Map2() {
        super(MainFrame.GAME_WIDTH, MainFrame.GAME_HEIGHT);
        
        init();
        playerStartPos = new Point(300, gameHeight-300-PlayerData.HEIGHT);
    }
    
    
    
    

    @Override
    public void init() {
        platforms.add(new Rectangle(100, gameHeight - 100, gameWidth - 200, 10));
        platforms.add(new Rectangle(100, gameHeight-710, 10, gameHeight - 150));
        platforms.add(new Rectangle(gameWidth - 100 - 10, gameHeight-710, 10, gameHeight - 150));
        platforms.add(new Rectangle(100, gameHeight - 300, gameWidth - 200, 10));
        platforms.add(new Rectangle(100, gameHeight - 500, gameWidth - 200, 10));
        platforms.add(new Rectangle(100, gameHeight - 710, gameWidth - 200, 10));
        platforms.add(new Rectangle(gameWidth/2, gameHeight-710, 10, 210));
        
        
        
        props.add(new FrontDoor(gameWidth-105-100, gameHeight-95-200, true));
        props.add(new Doors(gameWidth/2-Door.WIDTH, gameHeight-500-Door.HEIGHT, false, 
                            gameWidth/2+10, gameHeight-500-Door.HEIGHT, false));
        props.add(new Doors(gameWidth-105-Door.WIDTH, gameHeight-500-Door.HEIGHT, false, 
                            gameWidth-105-Door.WIDTH, gameHeight-295-Door.HEIGHT, false));
        props.add(new Doors(105, gameHeight-495, false, 
                            105, gameHeight-295, false));
        props.add(new Bed(110+100, gameHeight-500-Bed.HEIGHT));
        props.add(new LargeDresser(gameWidth/2-(gameWidth/6), gameHeight-500-LargeDresser.HEIGHT));
        props.add(new SmallTable(gameWidth/2+(gameWidth/8), gameHeight-500-SmallTable.HEIGHT));
        props.add(new Key(gameWidth/2+(gameWidth/8)+5, gameHeight-500-SmallTable.HEIGHT-Key.HEIGHT, Key.GOLD_KEY));
        props.add(new Bookshelf(gameWidth/2+(gameWidth/4), gameHeight-500-Bookshelf.HEIGHT));
        props.add(new Bookshelf(gameWidth/2+(gameWidth/4), gameHeight-500-Bookshelf.HEIGHT-Bookshelf.HEIGHT));
        props.add(new PottedPlant(gameWidth/2+(Pot.WIDTH/2), gameHeight-300-Pot.HEIGHT, 
                                  gameWidth/2+(Pot.WIDTH/2)-Plant.OFFSET, gameHeight-300-Pot.HEIGHT-Plant.HEIGHT));
        props.add(new LargeTable(gameWidth/2+(gameWidth/8), gameHeight-300-LargeTable.HEIGHT));
        props.add(new Computer(gameWidth/2+(gameWidth/8)+(LargeTable.WIDTH/2)-(Computer.WIDTH/2), gameHeight-300-LargeTable.HEIGHT-Computer.HEIGHT));
        props.add(new PottedPlant(gameWidth/2-(gameWidth/4), gameHeight-100-Pot.HEIGHT, 
                                  gameWidth/2-(gameWidth/4)-Plant.OFFSET, gameHeight-100-Pot.HEIGHT-Plant.HEIGHT));
        props.add(new LargeTable(gameWidth/2-(LargeTable.WIDTH/2), gameHeight-100-LargeTable.HEIGHT));
        props.add(new Chair(gameWidth/2-(LargeTable.WIDTH/2)-Chair.WIDTH-1, gameHeight-100-Chair.HEIGHT, PlayerData.RIGHT));
        props.add(new Chair(gameWidth/2+(LargeTable.WIDTH/2)+1, gameHeight-100-Chair.HEIGHT, PlayerData.LEFT));
        props.add(new SmallTable(gameWidth-210-SmallTable.WIDTH, gameHeight-100-SmallTable.HEIGHT));
        props.add(new Lamp(gameWidth-210-Lamp.WIDTH, gameHeight-100-SmallTable.HEIGHT-Lamp.HEIGHT));
        
        
        
        
        int[] enemyMovement = new int[] {Enemy.MOVE_LEFT, gameWidth/2-(gameWidth/4), Enemy.MOVE_RIGHT, gameWidth/2+(gameWidth/6)};
        enemies.add(new Enemy(gameWidth/2+(gameWidth/6), gameHeight-300-104, 0, enemyMovement));
        
        enemyMovement = new int[] {Enemy.MOVE_LEFT, 150, Enemy.MOVE_RIGHT, gameWidth-280};
        enemies.add(new Enemy(gameWidth-250, gameHeight-100-104, 2, enemyMovement));
        
        
        
        
        
        for (int i = 0; i < platforms.size(); i ++) {
            platforms.get(i).setColor(Color.BLACK);
        }
        
        isLoaded = true;
    }
    
}
