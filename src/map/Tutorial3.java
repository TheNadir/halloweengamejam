/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package map;

import entity.Enemy;
import java.awt.Color;
import java.awt.Font;
import java.awt.Point;
import libraries.Rectangle;
import libraries.Text;
import prop.FrontDoor;
import prop.Plant;
import prop.Pot;
import prop.PottedPlant;

/**
 *
 * @author Nikhil
 */
public class Tutorial3 extends Map {
    
    public Tutorial3() {
        super();
        
        init();
        playerStartPos = new Point(120, gameHeight-300-playerData.HEIGHT);
    }
    
    
    

    @Override
    public void init() {
        platforms.add(new Rectangle(100, gameHeight - 500, 10, 200));
        platforms.add(new Rectangle(gameWidth - 100 - 10, gameHeight - 500, 10, 200));
        platforms.add(new Rectangle(100, gameHeight - 300, gameWidth - 200, 10));
        platforms.add(new Rectangle(100, gameHeight - 500, gameWidth - 200, 10));
        
        
        
        props.add(new FrontDoor(gameWidth-105-FrontDoor.WIDTH, gameHeight-295-FrontDoor.HEIGHT));
        props.add(new PottedPlant(gameWidth/2-(Pot.WIDTH/2), gameHeight-295-Pot.HEIGHT, gameWidth/2-(Plant.WIDTH/2), gameHeight-295-Pot.HEIGHT-Plant.HEIGHT));
        
        
        
        
        int[] enemyMovement = {Enemy.MOVE_LEFT, gameWidth/4, Enemy.MOVE_RIGHT, gameWidth-gameWidth/4};
        enemies.add(new Enemy(gameWidth-105-FrontDoor.WIDTH, gameHeight-295-FrontDoor.HEIGHT, 0, enemyMovement));
        
        
        
        
        overlay.add(new Text(100, 200, new Font("Times New Roman", Font.PLAIN, 100), "Don't let the people catch you."));
        overlay.add(new Text(100, gameHeight-180, new Font("Times New Roman", Font.PLAIN, 60), "Hint: people are stupid and are creatures of habit."));
        
        
        
        
        for (int i = 0; i < platforms.size(); i ++) {
            platforms.get(i).setColor(Color.BLACK);
        }
        
        isLoaded = true;
    }
    
}
