/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package map;

import entity.Enemy;
import entity.PlayerData;
import halloween.game.jam.MainFrame;
import java.awt.Color;
import java.awt.Point;
import libraries.Rectangle;
import prop.Bed;
import prop.Chair;
import prop.Door;
import prop.Doors;
import prop.FrontDoor;
import prop.Key;
import prop.Lamp;
import prop.LargeDresser;
import prop.LargeTable;
import prop.PottedPlant;
import prop.SmallDresser;
import prop.SmallTable;

/**
 *
 * @author Nikhil
 */
public class BasicMap extends Map {
    
    public BasicMap(int gameWidth, int gameHeight) {
        super(gameWidth, gameHeight);
        
        init();
        playerStartPos = new Point(gameWidth/2, gameHeight/2);
    }
    public BasicMap() {
        super(MainFrame.GAME_WIDTH, MainFrame.GAME_HEIGHT);
        
        init();
        playerStartPos = new Point(MainFrame.GAME_WIDTH, MainFrame.GAME_HEIGHT);
    }
    
    
    
    
    
    @Override
    public void init() {
        platforms.add(new Rectangle(100, gameHeight - 100, gameWidth - 200, 10));
        platforms.add(new Rectangle(100, gameHeight-710, 10, gameHeight - 150));
        platforms.add(new Rectangle(gameWidth - 100 - 10, gameHeight-710, 10, gameHeight - 150));
        platforms.add(new Rectangle(100, gameHeight - 300, gameWidth - 200, 10));
        platforms.add(new Rectangle(100, gameHeight - 500, gameWidth - 200, 10));
        platforms.add(new Rectangle(100, gameHeight - 710, gameWidth - 200, 10));
        
        
        
        
        props.add(new FrontDoor(gameWidth-105-100, gameHeight-95-200, true));
        props.add(new Doors(gameWidth-105-100, gameHeight-295-200, true, 110, gameHeight-95-200, true));
        props.add(new Doors(110, gameHeight-295-200, 110, gameHeight-500-Door.HEIGHT));
        props.add(new SmallTable(gameWidth/4, gameHeight-295-39));
        props.add(new LargeTable(gameWidth/2, gameHeight-95-59));
        props.add(new Lamp(gameWidth/4, gameHeight-295-39-81));
        props.add(new Chair(gameWidth/2-50-1, gameHeight-95-92, PlayerData.RIGHT));
        props.add(new Chair(gameWidth/2+150+1, gameHeight-95-92, PlayerData.LEFT));
//        props.add(new Pot(gameWidth-gameWidth/4, gameHeight-295-50));
//        props.add(new Plant(gameWidth-gameWidth/4-30, gameHeight-295-50-129));
        props.add(new PottedPlant(gameWidth-gameWidth/4, gameHeight-295-50, gameWidth-gameWidth/4-30, gameHeight-295-50-129));
        props.add(new SmallDresser(gameWidth/4, gameHeight-95-59));
        props.add(new Key(gameWidth/4+100, gameHeight-295-Key.HEIGHT, Key.GOLD_KEY));
        props.add(new Key(gameWidth/4+100+Key.WIDTH, gameHeight-295-Key.HEIGHT, Key.SILVER_KEY));
        props.add(new Bed(gameWidth/4, gameHeight-500-Bed.HEIGHT));
        props.add(new LargeDresser(gameWidth/2, gameHeight-500-LargeDresser.HEIGHT));
        
        
        
        
        int[] enemyMovement = {Enemy.MOVE_LEFT, gameWidth/4, Enemy.MOVE_RIGHT, gameWidth/2};
        //enemies.add(new Enemy(gameWidth-105-100, gameHeight-95-110, 0, enemyMovement));
        enemies.add(new Enemy(gameWidth-105-100, gameHeight-95-110, 1, enemyMovement));
        
        
        
        
        for (int i = 0; i < platforms.size(); i ++) {
            platforms.get(i).setColor(Color.BLACK);
        }
        
        isLoaded = true;
    }
    
}
