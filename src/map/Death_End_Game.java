/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package map;

import entity.PlayerData;
import halloween.game.jam.MainFrame;
import java.awt.Point;
import libraries.Img;
import libraries.RotatedImg;
import prop.HangingPumpkin;

/**
 *
 * @author Nikhil
 */
public class Death_End_Game extends Map {

    public Death_End_Game() {
        super(MainFrame.GAME_WIDTH, MainFrame.GAME_HEIGHT);
        
        init();
        playerStartPos = new Point(gameWidth/2, gameHeight/2);
    }
    
    
    
    
    
    @Override
    public void init() {
        PlayerData.visible = false;
        
        props.add(new HangingPumpkin(MainFrame.GAME_WIDTH/2-150, MainFrame.GAME_HEIGHT/2-100));
        
        overlay.add(new Img(MainFrame.GAME_WIDTH/2-(300), MainFrame.GAME_HEIGHT/2-(537/2), 600, 537, "/libraries/images/Creepy_Tree.gif"));
        overlay.add(new RotatedImg(MainFrame.GAME_WIDTH/2-300, MainFrame.GAME_HEIGHT/2+(537/3), 100, 75, "/libraries/images/Hat.gif", 180));
        
        isLoaded = true;
    }
    
    @Override
    public void update() {
        for (int i = 0; i < props.size(); i ++) {
            props.get(i).update();
        }
        
        for (int i = 0; i < enemies.size(); i ++) {
            enemies.get(i).update();
        }
        
        PlayerData.visible = false;
    }
    
}
