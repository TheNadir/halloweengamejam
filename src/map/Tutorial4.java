/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package map;

import entity.PlayerData;
import halloween.game.jam.MainFrame;
import java.awt.Color;
import java.awt.Font;
import java.awt.Point;
import libraries.Rectangle;
import libraries.Text;
import prop.Door;
import prop.Doors;
import prop.FrontDoor;
import prop.Key;

/**
 *
 * @author Nikhil
 */
public class Tutorial4 extends Map {
    
    public Tutorial4() {
        super(MainFrame.GAME_WIDTH, MainFrame.GAME_HEIGHT);
        
        init();
        playerStartPos = new Point(120, gameHeight-(gameHeight/2)-PlayerData.HEIGHT);
    }
    
    
    
    

    @Override
    public void init() {
        platforms.add(new Rectangle(100, gameHeight-(gameHeight/2), gameWidth-200, 10));
        platforms.add(new Rectangle(100, gameHeight-(gameHeight/2)-210, gameWidth-200, 10));
        platforms.add(new Rectangle(100, gameHeight-(gameHeight/2)+210, gameWidth-200, 10));
        
        platforms.add(new Rectangle(90, gameHeight-(gameHeight/2)-210, 10, 430));
        platforms.add(new Rectangle(gameWidth-100, gameHeight-(gameHeight/2)-210, 10, 430));
        
        
        
        
        props.add(new Doors(gameWidth-100-Door.WIDTH, gameHeight-(gameHeight/2)-Door.HEIGHT, true, 
                            100, gameHeight-(gameHeight/2)+10, true));
        props.add(new Key(gameWidth/2, gameHeight-(gameHeight/2)-Key.HEIGHT, Key.SILVER_KEY));
        props.add(new FrontDoor(gameWidth-100-Door.WIDTH, gameHeight-(gameHeight/2)+10, true));
        props.add(new Key(gameWidth/2, gameHeight-(gameHeight/2)+210-Key.HEIGHT, Key.GOLD_KEY));
        
        
        
        
        overlay.add(new Text(150, gameHeight-(gameHeight/2)-260, new Font("Times New Roman", Font.PLAIN, 100), "Some doors require a key."));
        overlay.add(new Text(50, gameHeight-(gameHeight/2)+320, new Font("Times New Roman", Font.PLAIN, 100), "Match the key to the lock color."));
        
        
        
        
        
        
        for (int i = 0; i < platforms.size(); i ++) {
            platforms.get(i).setColor(Color.BLACK);
        }
        
        isLoaded = true;
    }
    
}
