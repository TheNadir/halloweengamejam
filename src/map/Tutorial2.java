/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package map;

import entity.PlayerData;
import java.awt.Color;
import java.awt.Font;
import java.awt.Point;
import libraries.Rectangle;
import libraries.Text;
import prop.FrontDoor;
import prop.Plant;
import prop.Pot;
import prop.PottedPlant;

/**
 *
 * @author Nikhil
 */
public class Tutorial2 extends Map {
    
    public Tutorial2(int gameWidth, int gameHeight) {
        super(gameWidth, gameHeight);
        
        init();
        playerStartPos = new Point(120, gameHeight-300-PlayerData.HEIGHT);
    }
    
    
    
    

    @Override
    public void init() {
        platforms.add(new Rectangle(100, gameHeight - 500, 10, 200));
        platforms.add(new Rectangle(gameWidth - 100 - 10, gameHeight - 500, 10, 200));
        platforms.add(new Rectangle(100, gameHeight - 300, gameWidth - 200, 10));
        platforms.add(new Rectangle(100, gameHeight - 500, gameWidth - 200, 10));
        
        
        props.add(new FrontDoor(gameWidth-105-FrontDoor.WIDTH, gameHeight-295-FrontDoor.HEIGHT));
        props.add(new PottedPlant(gameWidth/2, gameHeight-295-Pot.HEIGHT, gameWidth-gameWidth/2-30, gameHeight-295-Pot.HEIGHT-Plant.HEIGHT));
        
        
        overlay.add(new Text(50, 200, new Font("Times New Roman", Font.PLAIN, 90), "You can hide behind some objects."));
        overlay.add(new Text(80, gameHeight-150, new Font("Times New Roman", Font.PLAIN, 100), "Use 'X' to interact with objects."));
        
        
        
        
        for (int i = 0; i < platforms.size(); i ++) {
            platforms.get(i).setColor(Color.BLACK);
        }
        
        isLoaded = true;
    }
    
}
