/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prop;

import entity.PlayerData;
import libraries.Img;

/**
 *
 * @author Nikhil
 */
public abstract class Prop extends Img {
    
    // Global Variables
    public boolean solid = false;              // Does the player stop when in contact with this?
    public boolean hovering = false;           // Is the player in front of this?
    public boolean colliding = false;          // Is the player colliding with this?
    
    public PlayerData playerData = new PlayerData();
    
    
    
    
    public Prop() {
        super(0, 0, 10, 10, "/libraries/images/unsupportedTexture.png");
    }
    
    
    
    
    public abstract void update();
    
    public abstract void interact();
    
    
    
    
    public void initImg(int x, int y, int width, int height, String filePath) {
        setX(x);
        setY(y);
        setWidth(width);
        setHeight(height);
        setImage(filePath);
    }
    public void initImg(int x, int y, int width, int height) {
        setX(x);
        setY(y);
        setWidth(width);
        setHeight(height);
        setImage("/libraries/images/unsupportedTexture.png");
    }
    
}
