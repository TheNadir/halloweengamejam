/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prop;

import java.awt.Graphics;
import libraries.Element;

/**
 *
 * @author Nikhil
 */
public class PottedPlant extends Prop {
    
    // Global Variables
    private Pot pot;
    private Plant plant;
    
    

    public PottedPlant(Pot newPot, Plant newPlant) {
        pot = newPot;
        plant = newPlant;
        
        solid = true;
    }
    public PottedPlant(int potX, int potY, int plantX, int plantY) {
        pot = new Pot(potX, potY);
        plant = new Plant(plantX, plantY);
        
        solid = true;
    }
    
    
    
    
    @Override
    public void update() {
        // if statements so pot.colliding is not constantly being reset
        if (colliding && !(pot.colliding)) {
            pot.colliding = true;
        }
        else if (!(colliding) && pot.colliding) {
            pot.colliding = false;
        }
        
        pot.update();
        plant.update();
    }

    @Override
    public void interact() {
        pot.interact();
        plant.interact();
    }
    
    @Override
    public void draw(Graphics g) {
        pot.draw(g);
        plant.draw(g);
    }
    
    @Override
    public boolean isColliding(Element e) {
        return pot.isColliding(e) || plant.isColliding(e);
    }
    
}
