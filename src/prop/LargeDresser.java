/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prop;

import java.util.ArrayList;

/**
 *
 * @author Nikhil
 */
public class LargeDresser extends Prop {
    
    // Global Variables
    // Size Ratio - 56 : 76
    public static final int WIDTH = 93;
    public static final int HEIGHT = 127;
    
    private final String FILE_PATH = "/libraries/images/Dresser_Large.gif";
    
    
    
    public LargeDresser(int x, int y) {
        super();
        
        initImg(x, y, WIDTH, HEIGHT, FILE_PATH);
        solid = true;
    }
    
    
    

    @Override
    public void update() {
        if (colliding) {
            handleColliding();
        }
    }

    @Override
    public void interact() {
        
    }
    
    
    
    
    public void handleColliding() {
        ArrayList<Integer> collidingSides = playerData.getCollidingSides(this);
        
        // Only use the first one
        if (collidingSides.size() >= 1) {
            if (collidingSides.get(0) == playerData.DOWN) {
                playerData.setY(getY() - playerData.HEIGHT - 1);
            }
            else if (collidingSides.get(0) == playerData.RIGHT) {
                playerData.setX(getX() - playerData.WIDTH - 1);
            }
            else if (collidingSides.get(0) == playerData.LEFT) {
                playerData.setX(getX() + getWidth() + 1);
            }
        }
    }
    
}
