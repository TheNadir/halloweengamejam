/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prop;

import entity.PlayerData;
import java.awt.Point;

/**
 *
 * @author Nikhil
 */
public class Door extends Prop {
    
    // Global Variables
    public static final int WIDTH = 100;
    public static final int HEIGHT = 200;
    
    public final String FILE_PATH = "/libraries/images/Door.gif";
    public final String DOOR_OPEN_FILE_PATH = "/libraries/images/Door_Open.gif";
    public final String LOCKED_FILE_PATH = "/libraries/images/Door_Locked.gif";
    
    private boolean requiresKey = false;
    
    public Point moveTo = new Point(0, 0);
    
    
    
    
    public Door(int x, int y) {
        super();
        
        initImg(x, y, WIDTH, HEIGHT, FILE_PATH);
    }
    public Door(int x, int y, boolean newRequiresKey) {
        super();
        
        if (newRequiresKey) {
            initImg(x, y, WIDTH, HEIGHT, LOCKED_FILE_PATH);
        }
        else {
            initImg(x, y, WIDTH, HEIGHT, FILE_PATH);
        }
        requiresKey = newRequiresKey;
    }
    
    
    
    
    @Override
    public void update() {
        if (!(requiresKey) || (requiresKey && PlayerData.hasSilverKey)) {
            if (hovering && !(getFilePath().equals(DOOR_OPEN_FILE_PATH))) {
                setImage(DOOR_OPEN_FILE_PATH);
            }
            else if (!(hovering) && !(getFilePath().equals(FILE_PATH))) {
                if (requiresKey && !(getFilePath().equals(LOCKED_FILE_PATH))) {
                    setImage(LOCKED_FILE_PATH);
                }
                else if (!(requiresKey) && !(getFilePath().equals(FILE_PATH))) {
                    setImage(FILE_PATH);
                }
            }
        }
    }
    
    @Override
    public void interact() {
        if (!(requiresKey) || (requiresKey && PlayerData.hasSilverKey)) {
            playerData.moveTo(moveTo);
        }
    }
    
}
