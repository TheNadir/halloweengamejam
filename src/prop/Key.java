/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prop;

import entity.PlayerData;

/**
 *
 * @author Nikhil
 */
public class Key extends Prop {
    
    // Global Variables
    // Size Ratio - 11:21
    public static final int WIDTH = 40;
    public static final int HEIGHT = 75;
    
    public static final boolean GOLD_KEY = true;
    public static final boolean SILVER_KEY = false;
    
    private final String GOLD_FILE_PATH = "/libraries/images/Key_Gold.gif";
    private final String SILVER_FILE_PATH = "/libraries/images/Key_Silver.gif";
    
    private boolean keyColor;
    private boolean enabled = true;
    
    
    
    
    
    public Key(int x, int y, boolean newKeyColor) {
        super();
        
        keyColor = newKeyColor;
        if (keyColor == GOLD_KEY) {
            initImg(x, y, WIDTH, HEIGHT, GOLD_FILE_PATH);
        }
        else {
            initImg(x, y, WIDTH, HEIGHT, SILVER_FILE_PATH);
        }
    }
    
    
    
    
    

    @Override
    public void update() {
        if (hovering && enabled) {
            if (keyColor == GOLD_KEY) {
                PlayerData.hasGoldKey = true;
                setX(10);
                setY(10);
            }
            else {
                PlayerData.hasSilverKey = true;
                setX(15 + WIDTH);
                setY(10);
            }
            
            enabled = false;
        }
    }

    @Override
    public void interact() {
        
    }
    
}
