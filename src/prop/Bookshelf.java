/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prop;

/**
 *
 * @author Nikhil
 */
public class Bookshelf extends Prop {
    
    // Global Variables
    // Size Ratio - 31:27
    public static final int WIDTH = 93;
    public static final int HEIGHT = 81;
    
    private final String FILE_PATH = "/libraries/images/Bookshelf.gif";
    
    
    
    public Bookshelf(int x, int y) {
        super();
        
        initImg(x, y, WIDTH, HEIGHT, FILE_PATH);
    }
    
    
    
    

    @Override
    public void update() {
        
    }

    @Override
    public void interact() {
        
    }
    
}
