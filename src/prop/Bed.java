/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prop;

import java.util.ArrayList;

/**
 *
 * @author Nikhil
 */
public class Bed extends Prop {
    
    // Global Variables
    // Size Ratio - 32 : 13
    public static final int WIDTH = 155;
    public static final int HEIGHT = 63;
    
    private final String FILE_PATH = "/libraries/images/Bed.gif";
    
    
    
    public Bed(int x, int y) {
        super();
        
        initImg(x, y, WIDTH, HEIGHT, FILE_PATH);
        solid = true;
    }
    
    
    

    @Override
    public void update() {
        if (colliding) {
            handleColliding();
        }
    }

    @Override
    public void interact() {
        
    }
    
    
    
    
    public void handleColliding() {
        ArrayList<Integer> collidingSides = playerData.getCollidingSides(this);
        
        // Only handle when only the bottom is colliding with this
        // When player is on top of this
        if (collidingSides.size() == 1) {
            if (collidingSides.get(0) == playerData.DOWN) {
                playerData.setY(getY() - playerData.HEIGHT - 1);
            }
        }
    }
    
}
