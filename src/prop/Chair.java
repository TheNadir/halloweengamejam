/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prop;

/**
 *
 * @author Nikhil
 */
public class Chair extends Prop {
    
    // Global Variables
    // Size ratio - 6:11
    public static final int WIDTH = 50;
    public static final int HEIGHT = 92;
    public final String FILE_PATH_RIGHT = "/libraries/images/Chair_Right.gif";
    public final String FILE_PATH_LEFT = "/libraries/images/Chair_Left.gif";
    
    
    
    
    public Chair(int x, int y, int direction) {
        super();
        
        if (direction == playerData.RIGHT) {
            initImg(x, y, WIDTH, HEIGHT, FILE_PATH_RIGHT);
        }
        else {
            initImg(x, y, WIDTH, HEIGHT, FILE_PATH_LEFT);
        }
    }
    
    
    
    

    @Override
    public void update() {
        
    }

    @Override
    public void interact() {
        
    }
    
}
