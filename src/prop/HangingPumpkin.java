/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prop;

import java.awt.Graphics;
import java.awt.Point;
import libraries.RotatedImg;

/**
 *
 * @author Nikhil
 */
public class HangingPumpkin extends Prop {
    
    // Global Variables
    // Size Ratio - 24:59
    public final int WIDTH = 100;
    public final int HEIGHT = 246;
    public final String FILE_PATH = "/libraries/images/Pumpkin_Hanging.gif";

    private final int UPDATE_WAIT_TIME = 0;
    private final Point rotateAround;
    
    private RotatedImg pumpkin;
    private boolean swingingLeft = true;
    private long updateWaitTimer = System.currentTimeMillis();
    private double angle = 100;
    double angleAccel, angleVelocity = 0;
    double dt = 0.1;
    
    
    
    
    public HangingPumpkin(int x, int y) {
        super();
        
        rotateAround = new Point(WIDTH/2, y-HEIGHT-50);
        
        pumpkin = new RotatedImg(x, y, WIDTH, HEIGHT, FILE_PATH, 0, rotateAround);
    }
    
    
    

    @Override
    public void update() {
//        if (updateWaitTimer < System.currentTimeMillis()) {
//            if (swingingLeft) {
//                pumpkin.setAngle(pumpkin.getAngle() + DELTA_ANGLE);
//                if (pumpkin.getAngle() >= FAR_LEFT_ANGLE) {
//                    swingingLeft = false;
//                }
//            }
//            else {
//                pumpkin.setAngle(pumpkin.getAngle() - DELTA_ANGLE);
//                if (pumpkin.getAngle() <= FAR_RIGHT_ANGLE) {
//                    swingingLeft = true;
//                }
//            }
//            
//            updateWaitTimer = System.currentTimeMillis() + UPDATE_WAIT_TIME;
//        }
        

        
        if (updateWaitTimer < System.currentTimeMillis()) {
            angleAccel = (((-1) * 9.81) / HEIGHT) * Math.sin(angle);
            angleVelocity += angleAccel * dt;
            angle += angleVelocity * dt;
            
            pumpkin.setAngle(Math.toDegrees(angle));
            
            updateWaitTimer = System.currentTimeMillis() + UPDATE_WAIT_TIME;
        }
    }

    @Override
    public void interact() {
        
    }
    
    @Override
    public void draw(Graphics g) {
        pumpkin.draw(g);
    }
    
    
    
}
