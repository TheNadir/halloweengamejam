/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prop;

import entity.PlayerData;
import halloween.game.jam.LevelPicker;
import halloween.game.jam.MainFrame;
import javax.swing.JOptionPane;

/**
 *
 * @author Nikhil
 */
public class FrontDoor extends Prop {
    
    // Global Variables
    private final String FILE_PATH = "/libraries/images/Door_Front.gif";
    private final String OPEN_DOOR_FILE_PATH = "/libraries/images/Door_Front_Open.gif";
    private final String LOCKED_FILE_PATH = "/libraries/images/Door_Front_Locked.gif";
    
    private boolean requiresKey = false;
    
    public static final int WIDTH = 100;
    public static final int HEIGHT = 200;
    
    
    
    
    public FrontDoor(int x, int y) {
        super();
        
        initImg(x, y, WIDTH, HEIGHT, FILE_PATH);
    }
    public FrontDoor(int x, int y, boolean newRequiresKey) {
        super();
        
        if (newRequiresKey) {
            initImg(x, y, WIDTH, HEIGHT, LOCKED_FILE_PATH);
        }
        else {
            initImg(x, y, WIDTH, HEIGHT, FILE_PATH);
        }
        requiresKey = newRequiresKey;
    }

    
    
    
    
    @Override
    public void update() {
        if (!(requiresKey) || (requiresKey && PlayerData.hasGoldKey)) {
            if (hovering && !(getFilePath().equals(OPEN_DOOR_FILE_PATH))) {
                setImage(OPEN_DOOR_FILE_PATH);
            }
            else if (!(hovering) && !(getFilePath().equals(FILE_PATH))) {
                if (requiresKey && !(getFilePath().equals(LOCKED_FILE_PATH))) {
                    setImage(LOCKED_FILE_PATH);
                }
                else if (!(requiresKey) && !(getFilePath().equals(FILE_PATH))) {
                    setImage(FILE_PATH);
                }
            }
        }
    }
    
    @Override
    public void interact() {
        if (!(requiresKey) || (requiresKey && PlayerData.hasGoldKey)) {
            PlayerData.run = false;
            JOptionPane.showMessageDialog(null, "You successfully completed the Level!", "End Game", JOptionPane.INFORMATION_MESSAGE);
            MainFrame.switchPanel(new LevelPicker());
        }
    }
    
}
