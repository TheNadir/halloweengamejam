/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prop;

/**
 *
 * @author Nikhil
 */
public class Computer extends Prop {
    
    // Global Variables
    // Size Ratio - 18:23
    public static final int WIDTH = 50;
    public static final int HEIGHT = 64;
    
    private final String FILE_PATH = "/libraries/images/Computer.gif";
    
    
    
    public Computer(int x, int y) {
        super();
        
        initImg(x, y, WIDTH, HEIGHT, FILE_PATH);
    }
    
    
    

    @Override
    public void update() {
        
    }

    @Override
    public void interact() {
        
    }
    
}
