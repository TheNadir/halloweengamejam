/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prop;

import entity.PlayerData;
import java.util.ArrayList;
import libraries.Rectangle;

/**
 *
 * @author Nikhil
 */
public class SmallDresser extends Prop {
    
    // Global Variables
    private final String FILE_PATH = "/libraries/images/Dresser_Small.gif";
    
    // Size Ratio - 55:34
    public static final int WIDTH = 93;
    public static final int HEIGHT = 57;
    
    
    
    
    public SmallDresser(int x, int y) {
        super();
        
        initImg(x, y, WIDTH, HEIGHT, FILE_PATH);
        solid = true;
    }
    
    
    

    @Override
    public void update() {
        if (colliding) {
            handleColliding();
        }
    }

    @Override
    public void interact() {
        
    }
    
    
    
    
    public void handleColliding() {
        ArrayList<Integer> collidingSides = playerData.getCollidingSides(this);
        
        for (int i = 0; i < collidingSides.size(); i ++) {
            switch (collidingSides.get(i)) {
                case PlayerData.UP:
                    playerData.setY(getY() + getHeight());
                    break;
                case PlayerData.RIGHT:
                    playerData.setX(getX() - PlayerData.WIDTH);
                    break;
                case PlayerData.DOWN:
                    playerData.setY(getY() - PlayerData.HEIGHT);
                    break;
                case PlayerData.LEFT:
                    playerData.setX(getX() + getWidth());
                    break;
            }
        }
    }
    
}
