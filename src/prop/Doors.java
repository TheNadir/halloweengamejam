/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prop;

import java.awt.Graphics;
import libraries.Element;
import libraries.Rectangle;

/**
 *
 * @author Nikhil
 */
public class Doors extends Prop {
    
    // Global Variables
    private Door door1;
    private Door door2;
    
    
    
    public Doors(Door newDoor1, Door newDoor2) {
        door1 = newDoor1;
        door2 = newDoor2;
        
        initDoors();
    }
    public Doors(int door1X, int door1Y, int door2X, int door2Y) {
        door1 = new Door(door1X, door1Y);
        door2 = new Door(door2X, door2Y);
        
        initDoors();
    }
    public Doors(int door1X, int door1Y, boolean door1ReqKey, int door2X, int door2Y, boolean door2ReqKey) {
        door1 = new Door(door1X, door1Y, door1ReqKey);
        door2 = new Door(door2X, door2Y, door2ReqKey);
        
        initDoors();
    }
    
    

    @Override
    public void update() {
        door1.update();
        door2.update();
        
        // find which door player is hovering over
        if (hovering) {
            Rectangle tempPlayer = new Rectangle(playerData.getX(), playerData.getY(), 
                                             playerData.getWidth(), playerData.getHeight());
            
            if (tempPlayer.isColliding(door1) && !(door1.hovering)) {
                door1.hovering = true;
            }
            else if (tempPlayer.isColliding(door2) && !(door2.hovering)) {
                door2.hovering = true;
            }
        }
        else if (!(hovering) && (door1.hovering || door2.hovering)) {
            door1.hovering = false;
            door2.hovering = false;
        }
    }

    @Override
    public void interact() {
        Rectangle tempPlayer = new Rectangle(playerData.getX(), playerData.getY(), 
                                             playerData.getWidth(), playerData.getHeight());
        
        if (tempPlayer.isColliding(door1)) {
            door1.interact();
        }
        else if (tempPlayer.isColliding(door2)) {
            door2.interact();
        }
    }
    
    @Override
    public void draw(Graphics g) {
        door1.draw(g);
        door2.draw(g);
    }
    
    @Override
    public boolean isColliding(Element e) {
        return door1.isColliding(e) || door2.isColliding(e);
    }
    
    
    
    
    
    public void initDoors() {
        // center player in door for moveTo
        door1.moveTo.x = door2.getX() + (door2.getWidth()/2) - (playerData.getWidth()/2);
        door1.moveTo.y = door2.getY() + door2.getHeight() - playerData.getHeight();
        
        door2.moveTo.x = door1.getX() + (door1.getWidth()/2) - (playerData.getWidth()/2);
        door2.moveTo.y = door1.getY() + door1.getHeight() - playerData.getHeight();
    }
    
}
