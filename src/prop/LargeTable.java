/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prop;

import java.util.ArrayList;

/**
 *
 * @author Nikhil
 */
public class LargeTable extends Prop {
    
    // Global Variables
    // Size ratio - 8:3
    public static final int WIDTH = 150;
    public static final int HEIGHT = 57;
    private final String FILE_PATH = "/libraries/images/Table_Large.gif";
    private final String FILE_PATH_HIDDEN = "/libraries/images/Table_Large_Hidden_Player.gif";
    
    private boolean playerHiding = false;
    
    
    
    
    public LargeTable(int x, int y) {
        super();
        
        initImg(x, y, WIDTH, HEIGHT, FILE_PATH);
        solid = true;
    }

    
    
    
    
    @Override
    public void update() {
        if (colliding) {
            handleColliding();
        }
    }

    @Override
    public void interact() {
        if (!(playerHiding)) {
            playerData.visible = false;
            playerHiding = true;
            setImage(FILE_PATH_HIDDEN);
        }
        else {
            playerData.visible = true;
            playerHiding = false;
            setImage(FILE_PATH);
        }
    }
    
    
    
    
    public void handleColliding() {
        ArrayList<Integer> collidingSides = playerData.getCollidingSides(this);
        
        // Only handle when only the bottom is colliding with this
        // When player is on top of the table
        if (collidingSides.size() == 1) {
            if (collidingSides.get(0) == playerData.DOWN) {
                playerData.setY(getY() - playerData.HEIGHT - 1);
            }
        }
    }
    
}
