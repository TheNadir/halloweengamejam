/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prop;

/**
 *
 * @author Nikhil
 */
public class Lamp extends Prop {
    
    // Global Variables
    // Size ratio - 16:23
    public static final int WIDTH = 50;
    public static final int HEIGHT = 81;
    public final String FILE_PATH = "/libraries/images/Lamp_Off.gif";
    public final String ON_FILE_PATH = "/libraries/images/Lamp_On.gif";
    
    
    
    
    public Lamp(int x, int y) {
        super();
        
        initImg(x, y, WIDTH, HEIGHT, FILE_PATH);
    }
    
    
    
    

    @Override
    public void update() {
        
    }

    @Override
    public void interact() {
        if (getFilePath().equals(FILE_PATH)) {
            setImage(ON_FILE_PATH);
        }
        else {
            setImage(FILE_PATH);
        }
    }
    
}
