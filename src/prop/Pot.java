/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prop;

import java.util.ArrayList;

/**
 *
 * @author Nikhil
 */
public class Pot extends Prop {
    
    // Global Variables
    private final String FILE_PATH = "/libraries/images/Pot.gif";
    
    // Size ratio - 1:1
    public static final int WIDTH = 50;
    public static final int HEIGHT = 50;
    
    
    
    
    public Pot(int x, int y) {
        super();
        
        initImg(x, y, WIDTH, HEIGHT, FILE_PATH);
        solid = true;
    }
    
    
    

    @Override
    public void update() {
        if (colliding) {
            handleColliding();
        }
    }

    @Override
    public void interact() {
        
    }
    
    
    
    
    public void handleColliding() {
        ArrayList<Integer> collidingSides = playerData.getCollidingSides(this);
        // Only handle when only the bottom is colliding with this
        // When player is on top of this
        if (collidingSides.size() == 1) {
            if (collidingSides.get(0) == playerData.DOWN) {
                playerData.setY(getY() - playerData.HEIGHT - 1);
            }
        }
    }
    
}
