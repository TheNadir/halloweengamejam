/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prop;

import entity.PlayerData;

/**
 *
 * @author Nikhil
 */
public class Plant extends Prop {
    
    // Global Variables
    private final String FILE_PATH = "/libraries/images/Plant2.gif";
    private final String FILE_PATH_HIDDEN = "/libraries/images/Plant_Hidden_Player.gif";
    
    // Size Ratio - 13:14
    public static final int WIDTH = 119;
    public static final int HEIGHT = 129;
    public static final int OFFSET = 30;
    
    
    
    
    public Plant(int x, int y) {
        super();
        
        initImg(x, y, WIDTH, HEIGHT, FILE_PATH);
    }
    
    
    
    

    @Override
    public void update() {
        
    }

    @Override
    public void interact() {
        if (PlayerData.visible) {
            PlayerData.visible = false;
            setImage(FILE_PATH_HIDDEN);
        }
        else {
            PlayerData.visible = true;
            setImage(FILE_PATH);
        }
    }
    
}
