/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package halloween.game.jam;

import java.awt.event.ActionEvent;
import javax.swing.JButton;
import javax.swing.JPanel;

/**
 *
 * @author Nikhil
 */
public class MainMenu extends JPanel {
    
    public MainMenu() {
        JButton levelPickerBtn = new JButton("Level Picker");
        levelPickerBtn.addActionListener((ActionEvent ae) -> {
            switchToLevelPicker();
        });
        
        JButton quitBtn = new JButton("Quit");
        quitBtn.addActionListener((ActionEvent ae) -> {
            System.exit(0);
        });
        
        
        add(levelPickerBtn);
        add(quitBtn);
    }
    
    
    
    
    public void switchToLevelPicker() {
        MainFrame.switchPanel(new LevelPicker());
    }
    
}
