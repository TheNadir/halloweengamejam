/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package halloween.game.jam;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.JFrame;

/**
 *
 * @author Nikhil
 */
public class HalloweenGameJam {
    
    // Global Variables
    private static Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
    private final static int GAME_WIDTH = (int) dim.getWidth();
    private final static int GAME_HEIGHT = (int) dim.getHeight();
    

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        JFrame mainFrame = new JFrame("Halloween Game Jam");
        GamePanel gamePanel = new GamePanel();
        
        mainFrame.add(gamePanel);
        mainFrame.addKeyListener(new keyListener());
        KeyAdapter[] keyAdapters = gamePanel.getKeyAdapters();
        for (int i = 0; i < keyAdapters.length; i ++) {
            mainFrame.addKeyListener(keyAdapters[i]);
        }
        
        mainFrame.setSize(GAME_WIDTH, GAME_HEIGHT);
        mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        mainFrame.setLocationRelativeTo(null);
        mainFrame.setResizable(false);
        mainFrame.setVisible(true);
    }
    
    
    
    
    // Key Listener
    public static class keyListener extends KeyAdapter {
        @Override
        public void keyPressed(KeyEvent ke) {
            if (ke.getKeyCode() == KeyEvent.VK_ESCAPE) {
                System.exit(0);
            }
        }
    }
    
}
