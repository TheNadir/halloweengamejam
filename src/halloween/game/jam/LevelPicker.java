/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package halloween.game.jam;

import entity.PlayerData;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.JButton;
import javax.swing.JPanel;
import map.BasicMap;
import map.Map;
import map.Map1;
import map.Map2;
import map.Tutorial0;
import map.Tutorial1;
import map.Tutorial2;
import map.Tutorial3;
import map.Tutorial4;

/**
 *
 * @author Nikhil
 */
public class LevelPicker extends JPanel {
    
    public LevelPicker() {
        ArrayList<JButton> maps = new ArrayList<>();
        
        maps.add(new JButton("Tutorial1"));
        maps.add(new JButton("Tutorial2"));
        maps.add(new JButton("Tutorial3"));
        maps.add(new JButton("Tutorial4"));
        maps.add(new JButton("Tutorial5"));
        maps.add(new JButton("Map 1"));
        maps.add(new JButton("Map 2"));
        maps.add(new JButton("Testing"));
        
        
        
        
        
        
        
        
        
        for (int i = 0; i < maps.size(); i ++) {
            maps.get(i).addActionListener(createListener(maps.get(i).getText()));
            
            add(maps.get(i));
        }
        
        
        
        
        
//        JButton testBtn = new JButton("Hello World");
//        testBtn.addActionListener(new ActionListener() {
//            @Override
//            public void actionPerformed(ActionEvent e) {
//                System.exit(0);
//            }
//        });
//        
//        add(testBtn);
    }
    
    
    
    
    /**
     * Action Listener for Map Chooser Buttons
     * @param mapName
     * @return 
     */
    public ActionListener createListener(String mapName) {
        return new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                launchMap(mapName);
            }
        };
    }
    
    /**
     * Load the Map chosen into the game
     * @param mapName 
     */
    public void launchMap(String mapName) {
        PlayerData.run = false;
        Map map = new BasicMap(MainFrame.GAME_WIDTH, MainFrame.GAME_HEIGHT);
        switch (mapName) {
            case "Tutorial1":
                map = new Tutorial0(MainFrame.GAME_WIDTH, MainFrame.GAME_HEIGHT);
                break;
            case "Tutorial2":
                map = new Tutorial1(MainFrame.GAME_WIDTH, MainFrame.GAME_HEIGHT);
                break;
            case "Tutorial3":
                map = new Tutorial2(MainFrame.GAME_WIDTH, MainFrame.GAME_HEIGHT);
                break;
            case "Tutorial4":
                map = new Tutorial3();
                break;
            case "Tutorial5":
                map = new Tutorial4();
                break;
            case "Map 1":
                map = new Map1();
                break;
            case "Map 2":
                map = new Map2();
                break;
            case "Testing":
                map = new BasicMap(MainFrame.GAME_WIDTH, MainFrame.GAME_HEIGHT);
                break;
        }

        PlayerData.run = true;
        MainFrame.switchPanel(new GamePanel(map));
    }
    
    /**
     * Initialize Map variables
     * @param map 
     */
    public static void launchMap(Map map) {
        PlayerData.run = false;
        PlayerData.run = true;
        MainFrame.switchPanel(new GamePanel(map));
    }
    
}
