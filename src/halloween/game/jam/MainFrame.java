/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package halloween.game.jam;

import entity.PlayerData;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 *
 * @author Nikhil
 */
public class MainFrame {
    
    // Global Variables
    private static Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
    private static JFrame mainFrame = new JFrame();
    
    public static int GAME_WIDTH = (int) dim.getWidth();
    public static int GAME_HEIGHT = (int) dim.getHeight();
    
    
    
    
    public static void main(String[] args) {
        if (GAME_WIDTH > 1366) {
            GAME_WIDTH = 1366;
        }
        if (GAME_HEIGHT > 768) {
            GAME_HEIGHT = 768;
        }
        
        switchPanel(new MainMenu());
    }
    
    
    
    
    /**
     * Clear the JFrame and switch to a new menu
     * @param newPanel 
     */
    public static void switchPanel(JPanel newPanel) {
        if (mainFrame.isVisible()) {
            mainFrame.setVisible(false);;
            mainFrame.dispose();
        }
        mainFrame = new JFrame("Halloween Game Jam");
        
        
        
        mainFrame.add(newPanel);
        mainFrame.addKeyListener(new keyListener());
        KeyListener[] panelKeyListeners = newPanel.getKeyListeners();
        for (int i = 0; i < panelKeyListeners.length; i ++) {
            mainFrame.addKeyListener(panelKeyListeners[i]);
        }
        
        
        
        mainFrame.setSize(GAME_WIDTH, GAME_HEIGHT);
        mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        mainFrame.setLocationRelativeTo(null);
        mainFrame.setResizable(false);
        mainFrame.setVisible(true);
    }
    
    
    
    
    
    
    
    // Key Listener
    public static class keyListener extends KeyAdapter {
        @Override
        public void keyPressed(KeyEvent ke) {
            if (ke.getKeyCode() == KeyEvent.VK_ESCAPE) {
                System.exit(0);
            }
            if (ke.getKeyCode() == KeyEvent.VK_P) {
                switchPanel(new MainMenu());
            }
            if (ke.getKeyCode() == KeyEvent.VK_ENTER) {
                System.out.println(PlayerData.visible);
            }
        }
    }
    
}
