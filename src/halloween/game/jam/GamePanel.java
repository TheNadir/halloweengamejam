/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package halloween.game.jam;

import entity.Player;
import entity.PlayerData;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import libraries.Rectangle;
import map.BasicMap;
import map.Death_End_Game;
import map.Map;
import map.Tutorial0;
import prop.Prop;

/**
 *
 * @author Nikhil
 */
public class GamePanel extends JPanel implements Runnable {
    
    // Global Variables
    private final int UPDATE_SLEEP_TIME = 5;
    
    private Rectangle background;
    private Player player;
    private Map map = new BasicMap(MainFrame.GAME_WIDTH, MainFrame.GAME_HEIGHT);
    private boolean running = true;
    
    private PlayerData playerData = new PlayerData();
    
    
    
    
    
    public GamePanel() {
        // init map
        //map = new BasicMap(panelWidth, panelHeight);
        map = new Tutorial0(MainFrame.GAME_WIDTH, MainFrame.GAME_HEIGHT);
        
        initGame();
    }
    public GamePanel(Map newMap) {
        // initMap
        map = newMap;
        
        initGame();
        
        PlayerData.visible = true;
        System.out.println(PlayerData.visible);
    }
    
    
    
    
    /**
     * Initialize the game to default standards
     * Create the background, player, and start the Thread
     */
    public void initGame() {
        // init Player
        PlayerData.initPlayer();
        
        PlayerData.run = true;
        PlayerData.visible = true;
        
        // init background
        background = new Rectangle(0, 0, MainFrame.GAME_WIDTH, MainFrame.GAME_HEIGHT, Color.GRAY);
        
        // init player
        player = new Player(map.playerStartPos.x, map.playerStartPos.y);
        
        addListeners();
        
        new Thread(this).start();
    }
    
    
    
    
    
    /**
     * Runs the game
     * Contains the timer for the game's runtime
     */
    @Override
    public void run() {
        long tm = System.currentTimeMillis();
        
        while (PlayerData.run && running) {
            repaint();
            update(tm);
            
            
            colliding();
            
            
            try {
                tm += UPDATE_SLEEP_TIME;
                Thread.sleep(Math.max(0, tm - System.currentTimeMillis()));
            } catch (InterruptedException ex) {
                System.err.println("InterruptedException");
                ex.printStackTrace();
            }
        }
    }
    
    /**
     * Paint all elements to the canvas
     * @param g 
     */
    @Override
    public void paint(Graphics g) {
        super.paintComponent(g);                            // Clear the canvas
        
        background.draw(g);
        
        map.draw(g);
        
        if (PlayerData.visible) {
            player.draw(g);
        }
    }
    
    
    
    
    
    /**
     * Update any elements that require it
     * @param tm 
     */
    public void update(long tm) {
        player.update(tm);
        map.update();
    }
    
    /**
     * Handle all colliding elements
     */
    public void colliding() {
        // Player colliding with Walls
        if (map.isColliding(player)) {
            ArrayList<Rectangle> colliding = map.getColliding(player);
            for (Rectangle rect : colliding) {
                player.handleColliding(rect);
            }
        }
        
        // Player hovering in front of Props
        for (int i = 0; i < map.props.size(); i ++) {
            if (map.props.get(i).isColliding(player)) {
                map.props.get(i).hovering = true;
            }
            else {
                if (map.props.get(i).hovering) {            // include if statement to stop always resetting hovering to false
                    map.props.get(i).hovering = false;
                }
            }
            
            // Player colliding with solid Props
            // include else statement to stop always resetting colliding to false
            if (map.props.get(i).solid && map.props.get(i).isColliding(player)) {
                map.props.get(i).colliding = true;
            }
            else if (map.props.get(i).solid && !(map.props.get(i).isColliding(player)) && map.props.get(i).colliding) {
                map.props.get(i).colliding = false;
            }
        }
        
        for (int i = 0; i < map.enemies.size(); i ++) {
            // Player colliding with Enemy
            if (player.isColliding(map.enemies.get(i)) && PlayerData.visible) {
                endGame();
            }
            
            // Enemies colliding with Walls
            if (map.isColliding(map.enemies.get(i))) {
                ArrayList<Rectangle> colliding = map.getColliding(map.enemies.get(i));
                for (Rectangle rect : colliding) {
                    map.enemies.get(i).handleColliding(rect);
                }
            }
        }
    }
    
    /**
     * User is attempting to interact with a colliding element
     */
    public void interact() {
        for (Prop prop : map.props) {
            if (prop.isColliding(player)) {
                prop.interact();
            }
        }
    }
    
    /**
     * End Game Scenes
     */
    public void endGame() {
       PlayerData.run = false;
       running = false;
       JOptionPane.showMessageDialog(null, "Game Over", "End Game", JOptionPane.INFORMATION_MESSAGE);
       GamePanel endGamePanel = new GamePanel(new Death_End_Game());
       MainFrame.switchPanel(endGamePanel);
//       MainFrame.switchPanel(new GamePanel(new Death_End_Game()));
       
       int reply = JOptionPane.showConfirmDialog(null, "Continue?", "End Game", JOptionPane.YES_NO_OPTION);
       if (reply == JOptionPane.YES_OPTION) {
           endGamePanel.close();
           PlayerData.visible = true;
           MainFrame.switchPanel(new GamePanel(map));
       }
       else if (reply == JOptionPane.NO_OPTION) {
           MainFrame.switchPanel(new LevelPicker());
           endGamePanel.close();
       }
    }
    
    /**
     * Get all Key Listeners
     * @return 
     */
    public KeyAdapter[] getKeyAdapters() {
        return new KeyAdapter[]{new keyListener(), 
                                player.getKeyAdapter()};
    }
    
    /**
     * Add all Key Listeners to class Listeners
     */
    public void addListeners() {
        addKeyListener(new keyListener());
        addKeyListener(player.getKeyAdapter());
    }
    
    /**
     * Stop running the Game Timer
     */
    public void close() {
        running = false;
    }
    
    
    
    
    
    
    
    // Key Listener
    private class keyListener extends KeyAdapter {
        @Override
        public void keyPressed(KeyEvent ke) {
            switch (ke.getKeyCode()) {
                case KeyEvent.VK_P:                 // Pause
                    map.initMap();
                    break;
                case KeyEvent.VK_X:
                case KeyEvent.VK_E:                 // Interact
                    interact();
                    break;
            }
        }
    }
    
}
