/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.awt.Point;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import libraries.Element;
import libraries.Rectangle;

/**
 *
 * @author Nikhil
 */
public class Player extends Entity {
    
    // Global Variables
    private final int MOVE_NULL = 0;
    private final int MOVE_RIGHT = 1;
    private final int MOVE_LEFT = 2;
    private final int MOVE_DISTANCE = 2;
    private final int JUMP_SPEED = -20;
    private final int GRAVITY = 2;
    private final int TERMINAL_VELOCITY = 2;
    private final int JUMP_WAIT_TIME = 300;
    
    private ArrayList<Integer> moving = new ArrayList<>();
    private int velocityX = 0;
    private int velocityY = 0;
    private long jumpWaitUntil = 0;
    
    long currentTime = System.currentTimeMillis();
    
    PlayerData playerData = new PlayerData();
    
    
    
    
    public Player(int x, int y) {
        initImg(x, y, playerData.getWidth(), playerData.getHeight(), playerData.getFilePath(), 0);
        
        playerData.moveTo(new Point(x, y));
        PlayerData.visible = true;
    }
    
    
    
    
    @Override
    public void update(long tm) {
        currentTime = tm;
        
        // Have the player react to objects surrounding it before it moves
        setX(playerData.getPos().x);
        setY(playerData.getPos().y);
        
        // Don't move the player when it's not visible
        if (PlayerData.visible) {
            move();
        }
    }
    
    @Override
    public void move() {
        
        // Have the player react to objects surrounding it before it moves
        setX(playerData.getPos().x);
        setY(playerData.getPos().y);
        
        for (Integer move : moving) {
            switch (move) {
                case MOVE_NULL:
                    velocityX = 0;
                    break;
                case MOVE_RIGHT:
                    velocityX = MOVE_DISTANCE;
                    break;
                case MOVE_LEFT:
                    velocityX = (-1) * MOVE_DISTANCE;
                    break;
            }
        }
        
        // add gravity
        velocityY += GRAVITY;
        if (velocityY > TERMINAL_VELOCITY) {
            velocityY = TERMINAL_VELOCITY;
        }
        
        int newX = playerData.getPos().x + velocityX;
        int newY = playerData.getPos().y + velocityY;
        
        playerData.moveTo(new Point(newX, newY));
        
//        setX(playerData.getPos().x);
//        setY(playerData.getPos().y);
    }
    
    @Override
    public void handleColliding(Element e) {
        Rectangle[] border = new Rectangle[4];
        int os = 10;                // off set
        int size = 10;              // size of border
        
        // set borders
        border[0] = new Rectangle(getX() + os, getY(), 
                                  getWidth() - os - os, size);                      // Up
        border[1] = new Rectangle(getX() + getWidth() - size, getY() + os, size, 
                                  getHeight() - os - os);                           // Right
        border[2] = new Rectangle(getX() + os, getY() + getHeight() - size, 
                                  getWidth() - os - os, size);                      // Down
        border[3] = new Rectangle(getX(), getY() + os, 
                                  size, getHeight() - os - os);                     // Left

        if (border[0].isColliding(e)) {
            playerData.setY(e.getY() + e.getHeight() + 1);
        }
        if (border[2].isColliding(e)) {
            playerData.setY(e.getY() - getHeight() - 1);
        }
        if (border[1].isColliding(e)) {
            playerData.setX(e.getX() - getWidth() - 1);
        }
        if (border[3].isColliding(e)) {
            playerData.setX(e.getX() + e.getWidth() + 1);
        }
        
        setX(playerData.getPos().x);
        setY(playerData.getPos().y);
    }
    
    /**
     * Get all sides colliding with Element e
     * @param e
     * @return 
     */
    public ArrayList<Integer> getCollidingSides(Element e) {
        ArrayList<Integer> ret = new ArrayList<>();
        
        Rectangle[] border = new Rectangle[4];
        int os = 10;                // off set
        int size = 10;              // size of border
        
        // set borders
        border[0] = new Rectangle(getX() + os, getY(), 
                                  getWidth() - os - os, size);                      // Up
        border[1] = new Rectangle(getX() + getWidth() - size, getY() + os, size, 
                                  getHeight() - os - os);                           // Right
        border[2] = new Rectangle(getX() + os, getY() + getHeight() - size, 
                                  getWidth() - os - os, size);                      // Down
        border[3] = new Rectangle(getX(), getY() + os, 
                                  size, getHeight() - os - os);                     // Left

        if (border[0].isColliding(e)) {
            ret.add(playerData.UP);
        }
        if (border[2].isColliding(e)) {
            ret.add(playerData.DOWN);
        }
        if (border[1].isColliding(e)) {
            ret.add(playerData.RIGHT);
        }
        if (border[3].isColliding(e)) {
            ret.add(playerData.LEFT);
        }
        
        return ret;
    }
    
    
    
    
    
    private void addMoving(int direction) {
        for (Integer move : moving) {
            if (move == direction) {
                return;
            }
        }
        
        removeMoving(MOVE_NULL);
        moving.add(direction);
    }
    
    private void removeMoving(int direction) {
        for (int i = 0; i < moving.size(); i ++) {
            if (moving.get(i) == direction) {
                moving.remove(i);
                
                if (moving.isEmpty()) {
                    moving.add(MOVE_NULL);
                }
                
                return;
            }
        }
    }
    
    private void jump() {
        if (jumpWaitUntil < currentTime) {
            velocityY = JUMP_SPEED;
            jumpWaitUntil = currentTime + JUMP_WAIT_TIME;
        }
    }
    
    
    
    
    // Getters and Setters
    public KeyAdapter getKeyAdapter() {
        return new keyListener();
    }
    
    
    
    
    // KeyListener
    private class keyListener extends KeyAdapter {
        @Override
        public void keyPressed(KeyEvent ke) {
            // Moving
            switch (ke.getKeyCode()) {
                case KeyEvent.VK_RIGHT:
                case KeyEvent.VK_D:                 // Right
                    addMoving(MOVE_RIGHT);
                    break;
                case KeyEvent.VK_LEFT:
                case KeyEvent.VK_A:                 // Left
                    addMoving(MOVE_LEFT);
                    break;
            }
            
            // Jumping
            if (ke.getKeyCode() == KeyEvent.VK_SPACE) {
                // Can't jump if player is hidden
                if (PlayerData.visible) {
                    jump();
                }
            }
        }
        
        @Override
        public void keyReleased(KeyEvent ke) {
            // Moving
            switch (ke.getKeyCode()) {
                case KeyEvent.VK_RIGHT:
                case KeyEvent.VK_D:                 // Right
                    removeMoving(MOVE_RIGHT);
                    break;
                case KeyEvent.VK_LEFT:
                case KeyEvent.VK_A:                 // Left
                    removeMoving(MOVE_LEFT);
                    break;
            }
        }
    }
}
