/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.awt.Graphics;
import java.awt.Point;
import java.util.ArrayList;
import libraries.Animation;
import libraries.Element;
import libraries.Rectangle;

/**
 *
 * @author Nikhil
 */
public class Enemy extends Entity {
    
    // Global Variables
    // presets for different enemies
    // {FILE_PATH, WIDTH, HEIGHT, FRAME_WIDTH, FRAME_HEIGHT}
    private final Object[][] PRESETS = {{"/libraries/images/Enemy/Enemy_Jason_Map.gif", 104, 104, 100, 104}, 
                                        {"/libraries/images/Enemy/Enemy_Frankensteins_Monster_Map.gif", 80, 104, 80, 104}, 
                                        {"/libraries/images/Enemy/Enemy_Mike_Myers_Map.gif", 86, 100, 86, 100}};
    private final int INTERACT = 3;
    private final int GRAVITY = 2;
    private final int TERMINAL_VELOCITY = 2;
    private final int JUMP_SPEED = -20;
    private final int ANIMATION_UPDATE_TIME = 200;
    
    public static final int MOVE_DISTANCE = 1;
    public static final int MOVE_RIGHT = 0;
    public static final int MOVE_LEFT = 1;
    public static final int JUMP = 2;
    
    private int[] actions;
    private int actionCount = 0;
    private int velocityY = 0;
    private int preset = 0;
    
    private Animation animation;
    private int animationImgY = 0;
    
    
    
    
    
    public Enemy(int x, int y, int newPreset, int[] newActions) {
        super();
        
        initImg(x, y, (int) PRESETS[preset][1], (int) PRESETS[preset][2], PRESETS[preset][0].toString(), 0);
        actions = newActions;
        
        preset = newPreset;
        
        animation = new Animation(x, y, 
                                  (int) PRESETS[preset][1], (int) PRESETS[preset][2], 
                                  (int) PRESETS[preset][3], (int) PRESETS[preset][4], 
                                  ANIMATION_UPDATE_TIME, PRESETS[preset][0].toString(), 
                                  new Point(0, animationImgY));
    }
    public Enemy(int x, int y, int newPreset) {
        super();
        
        initImg(x, y, (int) PRESETS[preset][1], (int) PRESETS[preset][2], PRESETS[preset][0].toString(), 0);
        actions = null;
        
        preset = newPreset;
        
        animation = new Animation(x, y, 
                                  (int) PRESETS[preset][1], (int) PRESETS[preset][2], 
                                  (int) PRESETS[preset][3], (int) PRESETS[preset][4], 
                                  ANIMATION_UPDATE_TIME, PRESETS[preset][0].toString(), 
                                  new Point(0, animationImgY));
    }
    
    
    

    @Override
    public void update(long tm) {
        
    }
    
    /**
     * Update the game every iteration
     */
    public void update() {
        move();
        
        animation.update();
        if (animation.getDestroy()) {
            animation.setImgPoint(new Point(0, animationImgY));
            animation.setDestroy(false);
        }
        animation.setX(getX());
        animation.setY(getY());
    }
    
    @Override
    public void draw(Graphics g) {
        animation.draw(g);
    }

    @Override
    public void move() {
        // for moving left and right the next action block is for where they're moving to
        if (actions == null) {
            return;
        }
        if (actionCount >= actions.length) {
            actionCount = 0;
        }
        
        switch (actions[actionCount]) {
            case MOVE_LEFT:
                // If statement to not always reset imgPoint
                if (animationImgY != 0) {
                    animationImgY = 0;
                    animation.imgPoint.y = 0;
                }
                setX(getX() - MOVE_DISTANCE);
                if (getX() <= actions[actionCount+1]) {
                    actionCount += 2;
                }
                break;
            case MOVE_RIGHT:
                // If statement to not always reset imgPoint
                if (animationImgY != (int) PRESETS[preset][4]) {
                    animationImgY = (int) PRESETS[preset][4];
                    animation.imgPoint.y = (int) PRESETS[preset][4];
                }
                setX(getX() + MOVE_DISTANCE);
                if (getX() >= actions[actionCount+1]) {
                    actionCount += 2;
                }
                break;
            case JUMP:
                velocityY = JUMP_SPEED;
                actionCount ++;
                break;
            case INTERACT:
                actionCount ++;
                break;
        }
        
        // Add Gravity
        velocityY += GRAVITY;
        if (velocityY >= TERMINAL_VELOCITY) {
            velocityY = TERMINAL_VELOCITY;
        }
        
        setY(getY() + velocityY);
    }

    @Override
    public void handleColliding(Element e) {
        Rectangle[] border = new Rectangle[4];
        int os = 10;                // off set
        int size = 10;              // size of border
        
        // set borders
        border[0] = new Rectangle(getX() + os, getY(), 
                                  getWidth() - os - os, size);                      // Up
        border[1] = new Rectangle(getX() + getWidth() - size, getY() + os, size, 
                                  getHeight() - os - os);                           // Right
        border[2] = new Rectangle(getX() + os, getY() + getHeight() - size, 
                                  getWidth() - os - os, size);                      // Down
        border[3] = new Rectangle(getX(), getY() + os, 
                                  size, getHeight() - os - os);                     // Left

        if (border[0].isColliding(e)) {
            setY(e.getY() + e.getHeight() + 1);
        }
        if (border[2].isColliding(e)) {
            setY(e.getY() - getHeight() - 1);
        }
        if (border[1].isColliding(e)) {
            setX(e.getX() - getWidth() - 1);
        }
        if (border[3].isColliding(e)) {
            setX(e.getX() + e.getWidth() + 1);
        }
    }
    
    /**
     * Get all sides that are colliding with Element e
     * @param e
     * @return 
     */
    public ArrayList<Integer> getCollidingSides(Element e) {
        ArrayList<Integer> ret = new ArrayList<>();
        
        Rectangle[] border = new Rectangle[4];
        int os = 10;                // off set
        int size = 10;              // size of border
        
        // set borders
        border[0] = new Rectangle(getX() + os, getY(), 
                                  getWidth() - os - os, size);                      // Up
        border[1] = new Rectangle(getX() + getWidth() - size, getY() + os, size, 
                                  getHeight() - os - os);                           // Right
        border[2] = new Rectangle(getX() + os, getY() + getHeight() - size, 
                                  getWidth() - os - os, size);                      // Down
        border[3] = new Rectangle(getX(), getY() + os, 
                                  size, getHeight() - os - os);                     // Left

        if (border[0].isColliding(e)) {
            ret.add(PlayerData.UP);
        }
        if (border[2].isColliding(e)) {
            ret.add(PlayerData.DOWN);
        }
        if (border[1].isColliding(e)) {
            ret.add(PlayerData.RIGHT);
        }
        if (border[3].isColliding(e)) {
            ret.add(PlayerData.LEFT);
        }
        
        return ret;
    }
    
}
