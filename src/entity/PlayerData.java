/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.awt.Point;
import java.util.ArrayList;
import libraries.Element;
import libraries.Rectangle;

/**
 *
 * @author Nikhil
 */
public class PlayerData {
    
    // Global Variables
    public static final int WIDTH = 50;
    public static final int HEIGHT = 75;
    public static final String FILE_PATH = "/libraries/images/Player_Hat.gif";
    
    public static final int UP = 0;
    public static final int RIGHT = 1;
    public static final int DOWN = 2;
    public static final int LEFT = 3;
    
    public static Point pos;
    public static boolean visible = true;
    public static boolean hasGoldKey = false;
    public static boolean hasSilverKey = false;
    
    public static boolean run = true;
    
    
    
    
    public static void initPlayer() {
        visible = true;
        hasGoldKey = false;
        hasSilverKey = false;
    }
    
    public static void moveTo(Point moveToPos) {
        pos = moveToPos;
    }
    
    public ArrayList<Integer> getCollidingSides(Element e) {
        ArrayList<Integer> ret = new ArrayList<>();
        
        Rectangle[] border = new Rectangle[4];
        int os = 10;                // off set
        int size = 10;              // size of border
        
        // set borders
        border[0] = new Rectangle(getX() + os, getY(), 
                                  getWidth() - os - os, size);                      // Up
        border[1] = new Rectangle(getX() + getWidth() - size, getY() + os, size, 
                                  getHeight() - os - os);                           // Right
        border[2] = new Rectangle(getX() + os, getY() + getHeight() - size, 
                                  getWidth() - os - os, size);                      // Down
        border[3] = new Rectangle(getX(), getY() + os, 
                                  size, getHeight() - os - os);                     // Left

        if (border[0].isColliding(e)) {
            ret.add(UP);
        }
        if (border[2].isColliding(e)) {
            ret.add(DOWN);
        }
        if (border[1].isColliding(e)) {
            ret.add(RIGHT);
        }
        if (border[3].isColliding(e)) {
            ret.add(LEFT);
        }
        
        return ret;
    }
    
    
    
    
    // Getters and Setters
    public int getWidth() {
        return WIDTH;
    }
    
    public int getHeight() {
        return HEIGHT;
    }
    
    public String getFilePath() {
        return FILE_PATH;
    }
    
    public int getX() {
        return pos.x;
    }
    public void setX(int x) {
        pos.x = x;
    }
    
    public int getY() {
        return pos.y;
    }
    public void setY(int y) {
        pos.y = y;
    }
    
    public Point getPos() {
        return pos;
    }
    
}
