/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import libraries.Element;
import libraries.RotatedImg;

/**
 *
 * @author Nikhil
 */
public abstract class Entity extends RotatedImg {
    
    
    public Entity() {
        super(0, 0, 10, 10, null, 0);
    }
    
    
    
    public abstract void update(long tm);
    
    public abstract void move();
    
    public abstract void handleColliding(Element e);
    
    
    
    
    
    public void initImg(int x, int y, int width, int height, String filePath, double angle) {
        setX(x);
        setY(y);
        setWidth(width);
        setHeight(height);
        setImage(filePath);
        setAngle(angle);
    }
    
    public void checkColliding(Entity e) {
        if (this.isColliding(e)) {
            handleColliding(e);
        }
    }
    
}
