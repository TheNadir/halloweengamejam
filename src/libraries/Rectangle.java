/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package libraries;

import java.awt.Color;
import java.awt.Graphics;

/**
 *
 * @author sinhanr
 */
public class Rectangle extends Element {
    
    // Global Variables
    Color color;
    
    
    
    public Rectangle(int x, int y, int width, int height, Color newColor) {
        super(x, y, width, height);
        color = newColor;
    }
    
    public Rectangle(int x, int y, int width, int height) {
        super(x, y, width, height);
    }
    
    
    
    
    @Override
    public void draw(Graphics g) {
        g.setColor(getColor());
        g.fillRect(getX(), getY(), getWidth(), getHeight());
    }
    
    
    
    
    // Global Variables
    public Color getColor() {
        return color;
    }
    public void setColor(Color newColor) {
        color = newColor;
    }
}
