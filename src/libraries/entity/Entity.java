/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package libraries.entity;

import libraries.Element;
import libraries.Img;
import libraries.Rectangle;

/**
 *
 * @author Nikhil
 */
public abstract class Entity extends Img {
    
    // Global Variables
    private int[] hitboxOffset = {0, 0, 0, 0};
    
    public Entity() {
        super(0, 0, 0, 0, null);
    }
    
    
    
    
    public abstract void update();
    
    public abstract void move();
    
    public abstract void handleColliding(Element e);
    
    
    
    
    
    
    public void initImg(int x, int y, int width, int height, String filePath) {
        setX(x);
        setY(y);
        setWidth(width);
        setHeight(height);
        setImage(filePath);
    }
    
    public void checkColliding(Entity e) {
        Element hitbox = new Rectangle(getX() + hitboxOffset[3], getY() + hitboxOffset[1], getWidth() - hitboxOffset[1], getHeight() - hitboxOffset[2]);
        if (hitbox.isColliding(e)) {
            handleColliding(e);
        }
    }
}
