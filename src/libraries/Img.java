/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package libraries;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;
import javax.imageio.ImageIO;
import javax.swing.JOptionPane;

/**
 *
 * @author Nikhil
 */
public class Img extends Element {
    
    // Global Variables
    public static final String UNSUPPORTED_TEXTURE_FILE_PATH = "/libraries/images/unsupportedTexture.png";
    
    private Image img;
    private String filePath;
    
    
    
    public Img(int x, int y, int width, int height, String newFilePath) {
        super(x, y, width, height);
        
        setImage(newFilePath);
    }
    
    
    
    
    @Override
    public void draw(Graphics g) {
        g.drawImage(img, getX(), getY(), null);
    }
    
    
    
    
    // Getters and Setters
    public Image getImage() {
        return img;
    }
    public void setImage(String newFilePath) {
        filePath = newFilePath;
        if (newFilePath == null) {
            filePath = UNSUPPORTED_TEXTURE_FILE_PATH;
        }
        
        BufferedImage buffImg = null;
        try {
            buffImg = ImageIO.read(getClass().getResourceAsStream(filePath));
            img = buffImg.getScaledInstance(getWidth(), getHeight(), BufferedImage.SCALE_SMOOTH);
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, "Exception", filePath, JOptionPane.INFORMATION_MESSAGE);
            if (filePath.equals(UNSUPPORTED_TEXTURE_FILE_PATH)) {
                System.exit(0);
            }
            else {
                setImage(UNSUPPORTED_TEXTURE_FILE_PATH);
            }
        }
        
    }
    public void setImg(Image newImg) {
        img = newImg;
    }
    
    public String getFilePath() {
        return filePath;
    }

    public void setImgWidth(int newWidth) {
        setWidth(newWidth);
        img = img.getScaledInstance(getWidth(), getHeight(), Image.SCALE_SMOOTH);
    }
    
    public void setImgHeight(int newHeight) {
        setHeight(newHeight);
        img = img.getScaledInstance(getWidth(), getHeight(), Image.SCALE_SMOOTH);
    }
    
}
