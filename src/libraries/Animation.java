/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package libraries;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.Point;
import java.awt.image.BufferedImage;
import javax.imageio.ImageIO;
import javax.swing.JOptionPane;

/**
 *
 * @author Nikhil
 */
public class Animation extends Rectangle {
        
    // Global Variables
    private Image img;
    private int frameWidth, frameHeight;
    private int timer;                                  // 50
    private long nextFrameUpdate;
    private boolean destroy;
    
    public Point imgPoint;
    
    
    
    
    public Animation(int x, int y, int width, int height, int newFrameWidth, int newFrameHeight, int newTimer, String imgPath) {
        super(x, y, width, height);
        
        frameWidth = newFrameWidth;
        frameHeight = newFrameHeight;
        timer = newTimer;
        
//        ImageIcon ii = new ImageIcon(imgPath);
//        img = ii.getImage();
        checkImage(imgPath);
        
        imgPoint = new Point(0, 0);
        nextFrameUpdate = System.currentTimeMillis() + timer;
        destroy = false;
    }
    public Animation(int x, int y, int width, int height, int newFrameWidth, int newFrameHeight, int newTimer, String imgPath, Point startPoint) {
        super(x, y, width, height);
        
        frameWidth = newFrameWidth;
        frameHeight = newFrameHeight;
        timer = newTimer;
        
//        ImageIcon ii = new ImageIcon(imgPath);
//        img = ii.getImage();
        checkImage(imgPath);
        
        imgPoint = startPoint;
        nextFrameUpdate = System.currentTimeMillis() + timer;
        destroy = false;
    }
    
    
    
    
    @Override
    public void draw(Graphics g) {
        /*
        g.drawImage(img, getX(), getY(), 
                    getX() + getWidth(), getY() + getHeight(), 
                    imgPoint.x + frameWidth, imgPoint.y + frameHeight, 
                    imgPoint.x, imgPoint.y, null);
        */
        g.drawImage(img, getX(), getY(), 
                    getX() + getWidth(), getY() + getHeight(), 
                    imgPoint.x, imgPoint.y, 
                    imgPoint.x + frameWidth, imgPoint.y + frameHeight, null);
    }
    
    public void update() {
        long currentTime = System.currentTimeMillis();
        
        if (currentTime >= nextFrameUpdate) {
            imgPoint.x += frameWidth;
            nextFrameUpdate = System.currentTimeMillis() + timer;
            
            if (imgPoint.x >= img.getWidth(null)) {
                destroy = true;
            }
        }
    }
    
    
    
    
    private void checkImage(String filePath) {
        if (filePath == null) {
            filePath = Img.UNSUPPORTED_TEXTURE_FILE_PATH;
        }
        
        BufferedImage buffImg = null;
        try {
            buffImg = ImageIO.read(getClass().getResourceAsStream(filePath));
            img = buffImg.getScaledInstance(buffImg.getWidth(null), buffImg.getHeight(null), BufferedImage.SCALE_SMOOTH);
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, "Exception", filePath, JOptionPane.INFORMATION_MESSAGE);
            if (filePath.equals(Img.UNSUPPORTED_TEXTURE_FILE_PATH)) {
                System.exit(0);
            }
            else {
                checkImage(Img.UNSUPPORTED_TEXTURE_FILE_PATH);
            }
        }
    }
    
    
    
    
    
    // Getters and Setters
    public Image getImage() {
        return img;
    }
    public void setImage(String imgPath) {
        checkImage(imgPath);
    }
    public void setImage(Image newImg) {
        img = newImg;
    }
    
    public void setImgPoint(Point newPoint) {
        imgPoint = newPoint;
    }
    
    public boolean getDestroy() {
        return destroy;
    }
    public void setDestroy(boolean newDestroy) {
        destroy = newDestroy;
    }
    
}
