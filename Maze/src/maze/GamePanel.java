package Maze;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;
import javax.swing.JPanel;
import maze.Wall;
import resources.Element;
import resources.Rectangle;

/**
 *
 * @author Nikhil
 */
public class GamePanel extends JPanel implements Runnable {
    
    // Global Variables
    private final int UPDATE_SLEEP_TIME = 5;
    
    private ArrayList<Element> elements = new ArrayList<>();
    private ArrayList<Wall> walls = new ArrayList<>();
    
    public int x = 0;
    public int y = 0;
    
    public boolean running = true;
    
    
    
    public GamePanel(int panelWidth, int panelHeight) {
        init(panelWidth, panelHeight);
    }
    
    public GamePanel() {
        init(Main.GAME_WIDTH, Main.GAME_HEIGHT);
    }
    
    
    
    
    private void init(int panelWidth, int panelHeight) {
        // init background
        elements.add(new Rectangle(0, 0, panelWidth, panelHeight, Color.GRAY));
        
//        createWalls(new Point(0, 0), new Point(panelWidth, panelHeight));
        createWalls2(new Point(0, 0), new Point(panelWidth, panelHeight));
        
        new Thread(this).start();
    }
    
    
    
    
    @Override
    public void run() {
        long tm = System.currentTimeMillis();
        
        while (running) {
            repaint();
            update();
            
            
            
            try {
                tm += UPDATE_SLEEP_TIME;
                Thread.sleep(Math.max(0, tm - System.currentTimeMillis()));
            } catch (InterruptedException ex) {
                System.err.println("InterruptedException");
                ex.printStackTrace();
            }
        }
    }
    
    @Override
    public void paint(Graphics g) {
        super.paintComponent(g);                            // Clear the canvas
        
        Iterator elementsIterator = elements.iterator();
        Iterator wallsIterator = walls.iterator();
        
        while (elementsIterator.hasNext()) {
            ((Element) elementsIterator.next()).draw(g);
        }
        
        while (wallsIterator.hasNext()) {
            ((Wall) wallsIterator.next()).draw(g);
        }
    }
    
    
    
    
    
    public void update() {
        
    }
    
    public void createWalls(Point topLeft, Point bottomRight) {
        int widthCount = (bottomRight.x - (Wall.wallSize*2))/Wall.wallSize;
        int heightCount = (bottomRight.y - (Wall.wallSize*2))/Wall.wallSize;
        
        // If there is no room left for 2 walls - cancel recursion
        if (widthCount < 2 || heightCount < 2) {
            return;
        }
        
        int verticalHas2Halls = getRandNum(0, 1);
        
        int tempX = getRandNum(0, widthCount) * Wall.wallSize;
        int tempHall = getRandNum(1, heightCount-1) * Wall.wallSize;
        
        Wall verticalWall = new Wall(tempX, topLeft.y, bottomRight.y-topLeft.y, true, tempHall);
        
        int tempY = getRandNum(0, heightCount) * Wall.wallSize;
        do {
            tempHall = getRandNum(1, widthCount-1) * Wall.wallSize;
        } while (tempHall == tempX);
        
        Wall horizontalWall = new Wall(topLeft.y, tempY, bottomRight.x-topLeft.x, false, tempHall);
        
        if (verticalHas2Halls == 1) {           // Vertical Wall
            int topHall = getRandNum(0, (tempY/Wall.wallSize)-1);
            System.out.println("tempY: " + (tempY*Wall.wallSize));
            System.out.println("HeightCount: " + heightCount);
            System.out.println(heightCount-tempY+1);
            int bottomHall = getRandNum((tempY/Wall.wallSize)+1, heightCount);
            
            verticalWall = new Wall(tempX, topLeft.y, bottomRight.y-topLeft.y, true, topHall, bottomHall);
        }
        else {                                  // Horizontal Hall
            int topHall = getRandNum(0, (tempX/Wall.wallSize)-1);
            System.out.println("tempX: " + tempX);
            System.out.println("HeightCount: " + widthCount);
            System.out.println(widthCount-tempX+1);
            int bottomHall = getRandNum((tempX/Wall.wallSize)+1, widthCount);
            
            horizontalWall = new Wall(topLeft.y, tempY, bottomRight.x-topLeft.x, false, topHall, bottomHall);
        }
        
        walls.add(verticalWall);
        walls.add(horizontalWall);
        
        x = tempX;
        y = tempY;
        // Recurse to get full maze
        // Top Left
        // Top Right
        // Bottom Left
        // Bottom Right
    }
    
    public void createWalls2(Point topLeft, Point bottomRight) {
        int widthCount = (bottomRight.x - topLeft.x) / Wall.SIZE;
        int heightCount = (bottomRight.y - topLeft.y) / Wall.SIZE;
        
        // There is no room left for 2 walls - cancel recursion
        if (widthCount <= 2 || heightCount <= 2) {
            return;
        }
        
        int verticalHas2Halls = getRandNum(0, 1);
        
        int tempX = getRandNum(0, widthCount-1)* Wall.SIZE;
        int tempHall = getRandNum(0, heightCount) * Wall.SIZE;
        
        Wall verticalWall = new Wall(tempX, topLeft.y, bottomRight.y-topLeft.y, true, tempHall);
        
        int tempY;
        do {
            tempY = getRandNum(0, heightCount-1) * Wall.SIZE;
        } while (tempHall == tempY);
        tempHall = getRandNum(0, widthCount) * Wall.SIZE;
        
        Wall horizontalWall = new Wall(topLeft.x, tempY, bottomRight.x-topLeft.x, false, tempHall);
        
        if (verticalHas2Halls == 1) {       // Vertical has 2 halls
            System.out.println("Vertical");
            System.out.println((tempY-1)/Wall.SIZE);
            System.out.println((tempY+1)/Wall.SIZE);
            System.out.println(heightCount);
            
            int topHall = getRandNum(0, (tempY-1)/Wall.SIZE);
            int bottomHall = getRandNum((tempY+1)/Wall.SIZE, heightCount);
            
            verticalWall = new Wall(tempX, topLeft.y, bottomRight.y-topLeft.y, true, topHall, bottomHall);
        }
        else {                              // Horizontal has 2 halls
            System.out.println("Horizontal");
            System.out.println((tempX-1)/Wall.SIZE);
            System.out.println((tempX+1)/Wall.SIZE);
            System.out.println(widthCount);
            
            int leftHall = getRandNum(0, (tempX-1)/Wall.SIZE);
            int rightHall = getRandNum((tempX+1)/Wall.SIZE, widthCount);
            
            horizontalWall = new Wall(tempX, topLeft.y, bottomRight.y-topLeft.y, false, leftHall, rightHall);
        }
        
        walls.add(verticalWall);
        walls.add(horizontalWall);
        
        x = tempX;
        y = tempY;
    }
    
    public void createWalls3(Point topLeft, Point bottomRight) {
        int widthCount = (bottomRight.x - topLeft.x) / Wall.SIZE;
        int heightCount = (bottomRight.y - topLeft.y) / Wall.SIZE;
        
        // There is no room left for 2 walls - cancel recursion
        if (widthCount <= 4 || heightCount <= 4) {
            return;
        }
        
        int verticalHas2Halls = getRandNum(0, 1);
        
        int tempX = getRandNum(1, widthCount-1) * Wall.SIZE;
        int tempHall = getRandNum(0, heightCount) * Wall.SIZE;
        
        Wall verticalWall = new Wall(tempX, topLeft.y, bottomRight.y-topLeft.y, true, tempHall);
        
        int tempY;
        do {
            tempY = getRandNum(1, heightCount-1) * Wall.SIZE;
        } while (tempHall == tempY);
        do {
            tempHall = getRandNum(0, widthCount) * Wall.SIZE;
        } while (tempHall == tempX);
        
        Wall horizontalWall = new Wall(topLeft.x, tempY, bottomRight.x-topLeft.x, false, tempHall);
        
        if (verticalHas2Halls == 1) {       // Vertical has 2 halls
            System.out.println("Vertical");
            
            int topHall = getRandNum(0, (tempY-1)/Wall.SIZE);
            int bottomHall = getRandNum((tempY+1)/Wall.SIZE, heightCount-1);
            
            verticalWall = new Wall(tempX, topLeft.y, bottomRight.y-topLeft.y, true, topHall, bottomHall);
        }
        else {                              // Horizontal has 2 halls
            System.out.println("Horizontal");
            
            int leftHall = getRandNum(0, (tempX-1)/Wall.SIZE);
            int rightHall = getRandNum((tempX+1)/Wall.SIZE, widthCount);
            
            horizontalWall = new Wall(tempX, topLeft.y, bottomRight.y-topLeft.y, false, leftHall, rightHall);
        }
        
        walls.add(verticalWall);
        walls.add(horizontalWall);
        
        x = tempX;
        y = tempY;
    }
    
    public int getRandNum(int lowBound, int upBound) {
        Random rand = new Random();
        return rand.nextInt(upBound - lowBound + 1) + lowBound;
    }
    
    
    
    
    
    
    
    
    // Getters and Setters
    public KeyAdapter getKeyAdapter() {
        return new keyListener();
    }
    
    
    
    
    
    
    // Key Listener
    private class keyListener extends KeyAdapter {
        @Override
        public void keyPressed(KeyEvent ke) {
            if (ke.getKeyCode() == KeyEvent.VK_ENTER) {
//                walls.clear();
                //createWalls(new Point(0, 0), new Point(Main.GAME_WIDTH, Main.GAME_HEIGHT));
                createWalls2(new Point(0, 0), new Point(x, y));
            }
        }
    }
}
