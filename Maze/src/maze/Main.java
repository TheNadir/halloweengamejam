package Maze;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.JFrame;

/**
 *
 * @author Nikhil
 */
public class Main {
    
    // Global Variables
    private static final Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
    
    //public static final int GAME_WIDTH = (int) dim.getWidth();
    //public static final int GAME_HEIGHT = (int) dim.getHeight();
    public static final int GAME_WIDTH = 600;
    public static final int GAME_HEIGHT = 600;
    
    
    
    
    public static void main(String[] args) {
        JFrame mainFrame = new JFrame("GAME");
        GamePanel gamePanel = new GamePanel();
        
        mainFrame.add(gamePanel);
        mainFrame.addKeyListener(gamePanel.getKeyAdapter());
        mainFrame.addKeyListener(new keyListener());
        
        mainFrame.setSize(GAME_WIDTH+16, GAME_HEIGHT+39);             // Account for the window edges
        mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        mainFrame.setLocationRelativeTo(null);
        mainFrame.setResizable(true);
        mainFrame.setVisible(true);
    }
    
    
    
    
    
    // Key Listener
    private static class keyListener extends KeyAdapter{
        @Override
        public void keyPressed(KeyEvent ke) {
            if (ke.getKeyCode() == KeyEvent.VK_ESCAPE) {
                System.exit(0);
            }
        }
    }
}
