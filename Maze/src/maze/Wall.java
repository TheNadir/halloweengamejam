/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package maze;

import java.awt.Color;
import java.awt.Graphics;
import resources.Rectangle;

/**
 *
 * @author Nikhil
 */
public class Wall {
    
    // Global Variables
    public static final int wallSize = 10;
    public static final int SIZE = 10;
    
    private Rectangle rect1;
    private Rectangle rect2;
    private Rectangle rect3;
    
    
    
    
    
    public Wall(int x, int y, int length, boolean isVertical, int hall) {
        if (isVertical) {
            rect1 = new Rectangle(x, y, wallSize, hall, Color.BLACK);
            rect2 = new Rectangle(x, y+hall+wallSize, wallSize, length-hall-wallSize, Color.BLACK);
        }
        else {      // Horizontal
            rect1 = new Rectangle(x, y, hall, wallSize, Color.BLACK);
            rect2 = new Rectangle(x+hall+wallSize, y, length-hall-wallSize, wallSize, Color.BLACK);
        }
        
        rect3 = null;
    }
    
    public Wall(int x, int y, int length, boolean isVertical, int topHall, int bottomHall) {
        if (isVertical) {
            rect1 = new Rectangle(x, y, wallSize, topHall, Color.BLACK);
            rect2 = new Rectangle(x, y+topHall+wallSize, wallSize, bottomHall-topHall-wallSize, Color.BLACK);
            rect3 = new Rectangle(x, y+topHall+wallSize+bottomHall+wallSize, wallSize, length-topHall-wallSize-bottomHall-wallSize, Color.BLACK);
        }
        else {      // Horizontal
            rect1 = new Rectangle(x, y, wallSize, topHall, Color.BLACK);
            rect2 = new Rectangle(x+topHall+wallSize, y, bottomHall-topHall-wallSize, wallSize, Color.BLACK);
            rect3 = new Rectangle(x+topHall+wallSize+bottomHall+wallSize, y, length-topHall-wallSize-bottomHall-wallSize, wallSize, Color.BLACK);
        }
        
        rect1.setColor(Color.RED);
        rect2.setColor(Color.RED);
        rect3.setColor(Color.RED);
    }
    
    
    
    
    
    public void draw(Graphics g) {
        rect1.draw(g);
        rect2.draw(g);
        if (rect3 != null) {
            rect3.draw(g);
        }
    }
    
    
    
    
    
    // Getters and Setters
    public Rectangle getRect1() {
        return this.rect1;
    }
    
    public Rectangle getRect2() {
        return this.rect2;
    }
}
