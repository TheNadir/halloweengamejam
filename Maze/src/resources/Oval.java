/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package resources;

import java.awt.Color;
import java.awt.Graphics;

/**
 *
 * @author sinhanr
 */
public class Oval extends Element {
    
    // Global Variables
    private Color color;
    private int radius;
    
    
    
    
    public Oval(int x, int y, int width, int height, Color newColor) {
        super(x, y, width, height);
        color = newColor;
        int radius = width/2;
    }
    
    
    
    
    @Override
    public void draw(Graphics g) {
        g.setColor(getColor());
        g.fillOval(getX(), getY(), getWidth(), getHeight());
    }
    
    
    
    
    // Getters and Setters
    public Color getColor() {
        return color;
    }
    public void setColor(Color newColor) {
        color = newColor;
    }
    
    public int getRadius() {
        return radius;
    }
    
}
