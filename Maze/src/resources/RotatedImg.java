/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package resources;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.geom.AffineTransform;

/**
 *
 * @author Nikhil
 */
public class RotatedImg extends Img {
    
    // Global Variables
    private double angle;
    private Point rotateAround;
    private AffineTransform transform = new AffineTransform();
    
    
    
    
    public RotatedImg(int x, int y, int width, int height, String filePath, double newAngle) {
        super(x, y, width, height, filePath);
        
        angle = newAngle;
        // Rotate around center of image as default
        rotateAround = new Point(getWidth()/2, getHeight()/2);
    }
    public RotatedImg(int x, int y, int width, int height, String filePath, double newAngle, Point newRotateAround) {
        super(x, y, width, height, filePath);
        
        angle = newAngle;
        rotateAround = newRotateAround;
    }
    
    
    
    
    @Override
    public void draw(Graphics g) {
        Graphics2D g2d = (Graphics2D) g;
        
        transform.setToTranslation(getX(), getY());
        //transform.rotate(Math.toRadians(angle), getWidth()/2, getHeight()/2);
        transform.rotate(Math.toRadians(angle), rotateAround.x, rotateAround.y);
        
        g2d.drawImage(getImage(), transform, null);
    }
    
    
    
    
    // Getters and Setters
    public double getAngle() {
        return angle;
    }
    public void setAngle(double newAngle) {
        angle = newAngle;
    }
    
}
