/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package resources;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;

/**
 *
 * @author sinhads
 */
public class Text extends Element {
    
    // Global Variables
    private String text;
    private Color color;
    private Font font;
    
    
    
    public Text(int x, int y, Color newColor, Font newFont, String newText) {
        super(x, y, 0, 0);
        color = newColor;
        font = newFont;
        text = newText;
    }
    public Text(int x, int y, Color newColor, String newText) {
        super(x, y, 0, 0);
        color = newColor;
        font = new Font("Arial", Font.PLAIN, 50);
        text = newText;
    }
    public Text(int x, int y, Font newFont, String newText) {
        super(x, y, 0, 0);
        color = Color.BLACK;
        font = newFont;
        text = newText;
    }
    public Text(int x, int y, String newText) {
        super(x, y, 0, 0);
        color = Color.BLACK;
        font = new Font("Arial", Font.PLAIN, 50);
        text = newText;
    }
    
    
    
    
    
    @Override
    public void draw(Graphics g) {
        g.setFont(getFont());
        g.setColor(getColor());
        g.drawString(text, getX(), getY());
    }
    
    
    
    // Getters and Setters
    public String getText() {
        return text;
    }
    public void setText(String newText) {
        text = newText;
    }
    
    public Color getColor() {
        return color;
    }
    public void setColor(Color newColor) {
        color = newColor;
    }
    
    public Font getFont() {
        return font;
    }
    public void setFont(Font newFont) {
        font = newFont;
    }
            
}
