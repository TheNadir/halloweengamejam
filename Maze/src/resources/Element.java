/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package resources;

import java.awt.Graphics;

/**
 *
 * @author sinhanr
 */
public abstract class Element {
    
    // Global Variables
    private int x, y, width, height;
    
    
    
    
    public Element(int newX, int newY, int newWidth, int newHeight) {
        x = newX;
        y = newY;
        width = newWidth;
        height = newHeight;
    }
    
    public Element() {
        x = 0;
        y = 0;
        width = 0;
        height = 0;
    }
    
    
    
    
    public abstract void draw(Graphics g);
    
    public boolean isColliding(Element e) {
        int leftSide = getX();
        int rightSide = getX() + getWidth();
        int top = getY();
        int bottom = getY() + getHeight();
        
        int eLeftSide = e.getX();
        int eRightSide = e.getX() + e.getWidth();
        int eTop = e.getY();
        int eBottom = e.getY() + e.getHeight();
        
        
        if (eLeftSide < rightSide) {
            if (eRightSide > leftSide) {
                if (eTop < bottom) {
                    if (eBottom > top) {
                        return true;
                    }
                }
            }
        }
        
        
        return false;
    }
    
    
    
    
    // Getters and Setters
    public int getX() {
        return x;
    }
    public void setX(int newX) {
        x = newX;
    }
    
    public int getY() {
        return y;
    }
    public void setY(int newY) {
        y = newY;
    }
    
    public int getWidth() {
        return width;
    }
    public void setWidth(int newWidth) {
        width = newWidth;
    }
    
    public int getHeight() {
        return height;
    }
    public void setHeight(int newHeight) {
        height = newHeight;
    }
    
}
